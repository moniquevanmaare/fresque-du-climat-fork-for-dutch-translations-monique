---
title: 'Human activities'
backDescription: 'This is where it all begins…'
wikiUrl: 'https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_1_human_activities'
youtubeCode: 'fVcxeM0Ljh0'
instagramCode: 'CMALZpHns8Q'
lot: 1
num: 1
---

This is where it all begins... This is also where 5th IPCC report begins. In the foreword of the report, it is stated that "the IPCC is now 95 percent certain that human beings are the main cause of current global warming". This card can be considered: either as the cause of all the economic sector cards (Industry, Building Usage, Transportation, Agriculture), or as a heading for these cards (in which case they can be grouped together and circled).
