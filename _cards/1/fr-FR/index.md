---
title: 'Activités humaines'
backDescription: "C'est là que tout commence…"
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_1_activit%C3%A9s_humaines'
youtubeCode: 'ZPknjN0hYe8'
instagramCode: 'CMALNkEouzB'
lot: 1
num: 1
---

C'est par là que tout commence... C'est aussi par là que commence le 5ème rapport du GIEC, dans l'avant-propos de son résumé, il est mis en évidence que la science montre à présent avec 95 % de certitude que depuis le milieu du XXe siècle, l’activité humaine est la cause principale du réchauffement observé. Cette carte peut être considérée... soit comme la cause de l'ensemble des cartes secteurs économiques (Industrie, Utilisation des bâtiments, Transport, Agriculture), soit comme un titre de l'ensemble de ces cartes (qui sont alors regroupées dans une grosse “patate“).
