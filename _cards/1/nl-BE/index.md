---
lot: 1
num: 1
backDescription: Zo begint het…
title: Menselijke activiteiten.
---
Dit is waar het allemaal begint…Dit is ook waar het 5de IPCC-rapport mee begint. In het voorwoord van het rapport staat dat de IPCC nu 95% zeker is dat menselijk handelen de belangrijkste oorzaak is van de huidige opwarming van de aarde. Deze kaart kan gezien worden als de oorzaak van alle kaarten van de economische sector (Industrie, Gebruik van gebrouwen, Transport, Landbouw) of als _heading_ voor deze vier kaarten (die dan gegroepeerd en omcirkeld kunnen worden.)
