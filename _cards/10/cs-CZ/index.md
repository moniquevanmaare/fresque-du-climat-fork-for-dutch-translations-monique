---
title: 'Aerosoly'
backDescription: 'Se spreji toto nemá nic společného. Aerosoly jsou typem místního znečištění, které pochází z neúplného spalování fosilních paliv. Jsou škodlivé pro lidské zdraví a mají za následek záporné radiační působení (mají ochlazující vliv na zemské klima).'
lot: 3
num: 10
---
