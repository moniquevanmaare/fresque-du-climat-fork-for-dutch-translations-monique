---
title: 'Aerosol'
backDescription: 'Nessuna relazione con le bombolette spray. Gli aerosol sono un tipo di inquinamento locale, come il diossido di zolfo, che deriva dalla combustione incompleta dei combustibili fossili. Sono nocivi per la salute ed hanno un contributo negativo sul forzante'
lot: 3
num: 10
---
