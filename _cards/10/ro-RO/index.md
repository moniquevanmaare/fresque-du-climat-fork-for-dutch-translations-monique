---
title: 'Aerosoli'
backDescription: 'Nicio legătură cu spray-urile. Aerosolii reprezintă o poluare locală care rezultă din arderea incompletă a energiilor fosile. Sunt dăunători sănătății și contribuie negativ la forțarea radiativă, răcind clima.'
lot: 3
num: 10
---
