---
title: 'Aerosoler'
backDescription: 'Klimatmässigt har detta inget att göra med sprayflaskor. Aerosoler är en form av lokal förorening som uppkommer vid ofullständig förbränning av fossila bränslen. De har skadlig inverkan på mänsklig hälsa och bidrar negativt till strålningsdrivningen, vilk'
lot: 3
num: 10
---
