---
title: 'CO2-Konzentration (ppm)'
backDescription: 'Etwa die Hälfte unserer CO2-Emissionen wird von natürlichen Kohlenstoffsenken aufgenommen und gespeichert. Die andere Hälfte bleibt in der Atmosphäre; die Konzentration von CO2 in der Luft ist in 150 Jahren von 280 auf 410 ppm (parts per million) gestiege'
lot: 2
num: 11
---
