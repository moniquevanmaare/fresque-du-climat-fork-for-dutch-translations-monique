---
title: 'Concentrazione di CO2 (ppm)'
backDescription:
    "Una volta che la metà delle nostre emissioni di CO2 sono catturate dai pozzi naturali di assorbimento del carbonio, l'altra metà rimane nell'atmosfera.
    La concentrazione di CO2 è aumentata da 280 a 415 ppm (parti per milione) in 150 anni. Bisogna risalire"
lot: 2
num: 11
---
