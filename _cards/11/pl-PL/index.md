---
title: 'Stężenie CO2 (ppm)'
backDescription: 'O ile połowa naszych emisji CO2 zostaje pochłonięta przez naturalne rezerwuary węgla, to druga połowa pozostaje w atmosferze. Stężenie CO2 w atmosferze wzrosło z 280 do 415 ppm (liczba części na milion) w ciągu 150 lat. To najwyższy poziom w ciągu ostatni'
lot: 2
num: 11
---
