---
title: "Puits de carbone"
backDescription: "La moitié du CO2 que nous émettons chaque année est absorbée par les puits de carbone :
- la végétation pour 1/4 (via la photosynthèse)
- l'océan pour 1/4
Le reste (1/2) reste dans l'atmosphère."
wikiUrl: "https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_12_puits_de_carbone"
youtubeCode: "--H_a7k_sQ8"
instagramCode: "COA0IlZo3zu"
lot: 2
num: 12
---

Le graphique original du GIEC représente à la fois les émissions de CO2 et les puits de carbone. La Fresque a choisi de le scinder en deux afin de montrer d'une part d'où vient le CO2 et d'autre part où il va. C'est pourquoi les deux cartes sont exactement symétriques : chaque année, le CO2 émis par l'homme doit aller quelque part. Tout le CO2 qui n'est pas absorbé par les puits de carbone demeure dans l'atmosphère. Le texte à l'arrière de la carte donne la répartition approximative des taux d'absorption. Les pourcentages plus détaillés sont de : 27,9% pour l'océan, 28,8% pour la photosynthèse.
