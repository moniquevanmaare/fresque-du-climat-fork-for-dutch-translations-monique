---
title: "Pozzi di assorbimento di carbonio"
backDescription: "Metà della CO2 che emettiamo ogni anno viene assorbita dai pozzi naturali di assorbimento del carbonio:
-Vegetazione per 1/4 (fotosintesi)
-L'oceano per 1/4
Il resto (1/2) rimane nell'atmosfera."
lot: 2
num: 12
---
