---
title: "Karbonsluk"
backDescription: "Halvparten av alt CO2 vi slipper ut hvert år, absorberes av karbonsluk:
- 1/4 absorberes av vegetasjon (gjennom fotosyntese)
- 1/4 absorberes av havet
Resten (1/2) forblir i atmosfæren."
lot: 2
num: 12
---
