---
title: 'Koolstofreservoirs'
backDescription: 'De helft van de CO2 die we elk jaar uitstoten wordt opgeslagen in koolstofreservoirs.
    - 1/4 door de vegetatie (via fotosynthese)
    - 1/4 bij de oceaan
    De resterende helft blijft in de atmosfeer'
lot: 2
num: 12
---
De oorspronkelijke IPCC grafiek toont zowel CO2-emissies als koolstofreservoirs. Climate Fresk heeft ervoor gekozen dit in tweeën te splitsen om te laten zien waar de CO2 vandaan komt en waar het heengaat. Daarom zijn de kaarten symmetrisch: de CO2 die elk jaar uitgestoten wordt moet ergens terechtkomen. CO2 die niet geabsorbeerd wordt door de andere koolstofreservoirs blijft in de atmosfeer. De tekst op de achterkant van de kaart geeft een schatting van de absorbtiepercentages. Meer precieze cijfers voor de absorptie zijn: 27,9% in de oceaan en 28,8% door fotosynthese.
