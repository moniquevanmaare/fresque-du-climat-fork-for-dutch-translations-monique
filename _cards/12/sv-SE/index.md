---
title: "Kolsänkor"
backDescription: "Hälften av den koldioxid vi släpper ut årligen aborberas av kolsänkor:
- 1/4 av vegetation via fotosyntes
- 1/4 av havet
Den resterande halvan stannar i atmosfären."
lot: 2
num: 12
---
