---
backDescription: L'effet de serre est naturel. D’ailleurs, le premier GES naturel
    est la vapeur d’eau. Sans l'effet de serre, la planète serait 33°C plus froide.
    Mais le CO2 et les autres GES dus à l'Homme augmentent cet effet de serre naturel
    ce qui réchauffe le climat.
instagramCode: CJwB3kSImlo
lot: 1
num: 13
title: Efed ti-gwer ouzhpenn
wikiUrl: https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_13_effet_de_serre_additionnel
youtubeCode: M_bdkLnLQAE
---
