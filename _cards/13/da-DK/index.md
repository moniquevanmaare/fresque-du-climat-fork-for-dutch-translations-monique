---
title: 'Forøget Drivhuseffekt'
backDescription: 'Drivhuseffekten forekommer helt naturligt, uden den ville jordens temperatur være ca. 33˚C grader koldere, og jorden ville være ubeboelig for os. De stigende udslip af CO2 og andre drivhusgasser, som hovedsageligt skyldes menneskelige aktiviteter, forøger'
lot: 1
num: 13
---
