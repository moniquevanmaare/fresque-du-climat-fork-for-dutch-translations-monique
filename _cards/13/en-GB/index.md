---
title: "Additional Greenhouse Effect"
backDescription: "The greenhouse effect is a natural phenomenon -- and incidentally, the most common GHG is water vapour. Without the greenhouse effect, the planet would be 33°C colder and life as we know it would not be possible.
But CO2 and other GHGs related to human a"
wikiUrl: "https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_13_additional_greenhouse_effect"
youtubeCode: "1LxOZMKX8R8"
instagramCode: "CJwCN1ZHiJZ"
lot: 1
num: 13
---

On this card, we can see arrows of two colours: The orange arrows represent the energy that comes from the sun (UV, visible light and high-frequency infrared) and that which is reflected by the albedo effect at the same frequency. Albedo is the ability of a body to reflect light (a black body has an albedo of 0, a mirror has an albedo of 1, the earth has an average albedo of 0.31). The red arrows represent low frequency infrared energy, emitted by the Earth, which is less warm than the sun, or retained by the greenhouse effect. The greenhouse effect is based on the fact that it is not the same incoming radiation as outgoing radiation. On the right, -18°C is the temperature we would have on Earth without the greenhouse effect and 15°C is the planet's average temperature today. It was 14°C in 1850, i.e. before human activity produced this additional greenhouse effect.
