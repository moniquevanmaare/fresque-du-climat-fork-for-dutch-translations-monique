---
title: 'Efecto invernadero adicional'
backDescription: 'El efecto invernadero es un fenómeno natural. De hecho, el principal GEI natural es el vapor de agua. Sin el efecto invernadero la temperatura del planeta sería 33°C más fría. Pero las emisiones de CO2 y los otros GEI derivados de las actividades humanas'
lot: 1
num: 13
---
