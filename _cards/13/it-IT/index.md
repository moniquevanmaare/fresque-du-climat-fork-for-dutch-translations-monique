---
title: 'Effetto serra addizionale'
backDescription: "L'effetto serra è naturale. Infatti, il primo gas serra naturale è il vapore acqueo. Senza l'effetto serra, il pianeta sarebbe 33°C più freddo. Tuttavia il CO2 e gli altri gas a effetto serra dovuti all'uomo fanno aumentare questo effetto naturale riscald"
lot: 1
num: 13
---
