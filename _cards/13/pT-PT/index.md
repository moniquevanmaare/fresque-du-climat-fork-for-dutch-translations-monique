---
title: "Efeito de estufa adicional"
backDescription: "O efeito estufa é um fenómeno natural (a propósito, o principal GEE é o vapor de água). Sem efeito de estufa, o planeta seria 33° C mais frio e a vida tal como a conhecemos não seria possível.
Mas o CO2 e outros GEE inerentes às atividades humanas aument"
lot: 1
num: 13
---
