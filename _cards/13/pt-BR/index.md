---
title: 'Efeito Estufa Adicional'
backDescription: 'O efeito estufa é um fenômeno natural (a propósito, o principal GEE é o vapor de água). Sem efeito estufa, o planeta seria 33° C mais frio. Mas o CO2 e outros GEE relacionados às atividades humanas aumentam o efeito estufa natural e aquecem o clima.'
lot: 1
num: 13
---
