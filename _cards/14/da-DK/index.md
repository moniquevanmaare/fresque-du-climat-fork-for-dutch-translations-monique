---
title: 'Jordens energi-budget'
backDescription: 'De menneskeskabte drivhusgasser forrykker strålingsbalancen - mellem indgående og udgående stråling/energi. I denne graf kan man se, hvad der sker med den ekstra energi. Den opvarmer havene, smelter is og gletsjere, forsvinder ned i jorden og opvarmer atm'
lot: 3
num: 14
---
