---
title: 'Energiebilanz der Erde'
backDescription: 'Diese Grafik erklärt, wohin die Energie geht, die sich durch den Strahlungsantrieb auf der Erde angesammelt hat: Sie erwärmt den Ozean, lässt Eis schmelzen, geht in den Boden und erwärmt die Atmosphäre.'
lot: 3
num: 14
---
