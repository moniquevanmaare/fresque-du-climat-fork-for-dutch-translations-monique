---
title: 'Ενεργειακό ισοζύγιο'
backDescription: 'Αυτό το γράφημα εξηγεί πού πηγαίνει η ενέργεια που συσσωρεύεται στη Γη λόγω θετικής ακτινοβολιακής έντασης: θερμαίνει τους ωκεανούς, λιώνει τον πάγο, διασκορπίζεται στο έδαφος και θερμαίνει την ατμόσφαιρα.'
lot: 3
num: 14
---
