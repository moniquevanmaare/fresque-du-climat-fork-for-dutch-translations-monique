---
title: 'Energy Budget'
backDescription: 'This graph explains where the energy accumulated on Earth due to radiative forcing goes: it warms up the ocean, melts ice, dissipates into the ground and warms up the atmosphere.'
lot: 3
num: 14
---
