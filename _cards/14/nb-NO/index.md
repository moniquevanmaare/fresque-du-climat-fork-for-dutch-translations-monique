---
title: 'Strålingsbalanse'
backDescription: 'Denne grafen viser hvor energien som samles opp på kloden fra strålingspådriv havner: den varmer opp havet, smelter is, forsvinner i jorda og varmer opp atmosfæren.'
lot: 3
num: 14
---
