---
title: 'Energiebudget'
backDescription: 'Deze grafiek toont wat er met de geaccumuleerde energie (geaccumuleerd vanwege stralingsforcering) gebeurt: het warmt de oceanen op, doet het ijs smelten, verdwijnt in de grond en warmt de atmosfeer op.'
lot: 3
num: 14
---
In deze grafiek zie je in lichtblauw de hoeveelheid energie die door de bovenste laag van de oceaan opgenomen wordt, tussen de 0 een 700m diep. In donkerblauw, de lagere delen van de oceaan, tussen de 700m en 2000m diep. In wit de hoeveelheid in de verschillende soorten ijs. In oranje de hoeveelheid in de grond. In paars, de atmosfeer. De gestippelde lijn laat de totale hoeveelheid energie zien. 
