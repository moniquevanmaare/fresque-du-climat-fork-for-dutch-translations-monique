---
title: 'Bilans energetyczny'
backDescription: 'Ten wykres pokazuje, dokąd trafia energia skumulowana na powierzchni Ziemi w efekcie wymuszania radiacyjnego: podnosi ona temperaturę oceanów, powoduje topnienie pokrywy lodowej, wnika w głąb lądów i ogrzewa atmosferę.'
lot: 3
num: 14
---
