---
title: 'Bilanțul energetic'
backDescription: 'Acest grafic explică unde merge energia care se acumulează pe pământ datorită forțării radiative: încălzește oceanul, topește gheața, se disipează în pământ și încălzește atmosfera.'
lot: 3
num: 14
---
