---
title: 'Energetski bilans'
backDescription: 'Ovaj grafikon prikazuje gde odlazi energija uskladištena na Zemlji usled disbalansa zračenja: zagreva okeane, topi led, gubi se u tlu i zagreva atmosferu.'
lot: 3
num: 14
---
