---
title: 'Energibudget'
backDescription: 'Grafen förklarar vart den energi som ackumulerats på jorden tack vare strålningsdrivningen tar vägen: den värmer havet, smälter is, sprids i marken och värmer upp atmosfären.'
lot: 3
num: 14
---
