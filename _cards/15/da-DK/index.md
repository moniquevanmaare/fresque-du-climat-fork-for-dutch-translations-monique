---
title: 'Strålingspåvirkning'
backDescription: 'Strålingspåvirkning er et mål for den menneskeskabte forskel mellem den energi, der går ind i systemet (solindstråling), og den der går ud igen. Måles i W/m2 (watt pr. kvadratmeter). I IPCC’s 5. Assessment Report er den beregnet til 2.3 W/m2.'
lot: 3
num: 15
---
