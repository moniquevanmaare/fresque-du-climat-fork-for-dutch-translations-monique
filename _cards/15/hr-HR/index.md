---
title: "Poremećaj bilance zračenja (Radiative forcing)"
backDescription: "Poremećaj bilance zračenja je razlika između energije koja svake sekunde dospije na Zemlju i energije koja se oslobađa.

Procijenjena je na 2,8 W/m² (Watt po kvadratnom metru): 3,8 W/m² od stakleničkih plinova i -1 W/m² od aerosola."
lot: 3
num: 15
---
