---
title: 'Forzante radiativo'
backDescription: "Il forzante radiativo è la misura dello squilibrio tra l'energia che arriva ogni secondo sulla terra e quella che viene rilasciata. Corrisponde a 3,8 W/m ² (Watt per m²) per effetto serra e -1 W/m ² per gli aerosol, cioè 2,8 W/m ² in tutto."
lot: 3
num: 15
---
