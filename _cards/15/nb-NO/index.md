---
title: 'Strålingspådriv'
backDescription: 'Strålingspådriv måler ubalansen mellom inngående og utgående energi fra jorda hvert sekund. I IPCCs femte hovedrapport vurderes strålingspådriv til 2,3 W/m² (watt per kvadratmeter).'
lot: 3
num: 15
---
