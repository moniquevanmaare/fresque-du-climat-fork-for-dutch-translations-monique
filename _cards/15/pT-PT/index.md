---
title: 'Forçamento Radiativo'
backDescription: 'O Forçamento Radiativo representa a diferença entre a energia que chega à Terra a cada segundo e a energia que é libertada. É avaliado em 2,8 W/m² (Watt por metro quadrado), 3,8 W/m² do efeito de estufa e -1 W/m² dos aerossóis.'
lot: 3
num: 15
---
