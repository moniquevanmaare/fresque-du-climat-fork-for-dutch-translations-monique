---
title: 'Forțarea radiativă'
backDescription: 'Forțarea radiativă reprezintă diferența (cauzată de ființa umană) dintre energia care ajunge pe pământ în fiecare secundă și energia care este eliberată de el. În cel de-al cincilea raport de evaluare al IPCC aceasta este evaluată la 2,3 W / m² (Watt pe m'
lot: 3
num: 15
---
