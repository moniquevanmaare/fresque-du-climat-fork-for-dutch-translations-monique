---
title: 'Tání ledovců'
backDescription: 'Téměř všechny ledovce ubývají. Stovky z nich zmizely zcela. Ledovce přitom mají regulační úlohu v dostupnosti sladké vody.'
lot: 3
num: 16
---
