---
title: 'Fonte des glaciers'
backDescription: "Presque tous les glaciers ont perdu de la masse. Des centaines ont même déjà disparu. Or ces glaciers ont un rôle régulateur sur l'approvisionnement en eau douce."
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_16_fonte_des_glaciers'
youtubeCode: 'zRlMGkImeUs'
instagramCode: 'CMkOau-o86_'
lot: 3
num: 16
---

On parle ici des glaciers dans les montagnes. Techniquement, ce sont des cours d'eau, mais gelés, donc avec une viscosité beaucoup plus grande que l'eau.
