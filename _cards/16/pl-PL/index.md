---
title: 'Topnienie lodowców'
backDescription: 'Prawie wszystkie lodowce straciły na objętości, a setki nawet zanikły. Tymczasem lodowce pełnią rolę regulatora zasobów słodkiej wody.'
lot: 3
num: 16
---
