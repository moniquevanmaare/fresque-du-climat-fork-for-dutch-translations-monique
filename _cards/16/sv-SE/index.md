---
title: 'Smältning av glaciärer'
backDescription: 'Nästan alla glaciärer har förlorat massa, och hundratals av dem har försvunnit. Glaciärer spelar en viktig roll som reglerande källa för sötvatten'
lot: 3
num: 16
---
