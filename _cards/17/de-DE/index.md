---
title: 'Anstieg der Wassertemperatur'
backDescription: 'Der Ozean absorbiert 93% der auf der Erde angesammelten Energie. Die Wassertemperatur ist daher angestiegen. Wenn sich Wasser erwärmt, dehnt es sich aus.'
lot: 3
num: 17
---
