---
title: 'Increase in Water Temperature'
backDescription: 'Oceans absorb 93% of the energy accumulated on Earth. Their temperature has therefore increased, especially in the upper layers. The water expands as it warms up.'
lot: 3
num: 17
---
