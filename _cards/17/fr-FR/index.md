---
title: "Hausse de la température de l'eau"
backDescription: "L'océan absorbe 91% de l’énergie qui s’accumule sur la Terre. En se réchauffant, l’eau se dilate."
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_17_hausse_temp%C3%A9rature_eau'
youtubeCode: 'TRhhK8tJ9ds'
instagramCode: ''
lot: 3
num: 17
---

L'océan se réchauffe de l'ordre du dixième de degré en surface et encore moins en profondeur. Comment cela se fait-il alors qu'il absorbe 91% de l'énergie en excès sur la terre ? C'est parce qu'il est beaucoup plus gros que l'atmosphère. Il a une capacité calorifique beaucoup plus grande. Pour mesurer cela, il faut se rappeler que l'océan couvre 71% de la surface de la terre et qu'il a une profondeur de 4000m en moyenne. L'atmosphère s'étend sur une plus grande hauteur, mais si on le ramenait à la même densité que l'eau, il ne mesurerait plus que 10m (c'est pour ça qu'on gagne un atmosphère de pression tous les 10m quand on fait de la plongée). L'eau se dilate très peu. Comment le réchauffement de l'océan de l'ordre d'un dixième de degré peut-il se traduire par un hausse du niveau de l'eau ? Une première réponse consiste à dire que l'océan fait 4000m de profondeur en moyenne, et donc une toute petite dilatation suffit à faire quelques centimètre. L'explication plus complète fait l'objet d'une fiche pratique.
