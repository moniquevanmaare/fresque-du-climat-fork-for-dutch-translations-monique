---
title: 'Vízhőmérséklet-emelkedés'
backDescription: 'Az óceánok a Földön felhalmozódott energia 93%-át elnyelik. Ennek köszönhetően hőmérsékletük emelkedik, különösképpen a felső rétegekben. A víz ahogy melegszik, úgy tágul.'
lot: 3
num: 17
---
