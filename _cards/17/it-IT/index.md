---
title: "Aumento della temperatura dell'acqua"
backDescription: "L'oceano assorbe il 93% dell'energia che si accumula sulla terra. Quando si riscalda, l'acqua si dilata."
lot: 3
num: 17
---
