---
title: 'Økning i vanntemperatur'
backDescription: 'Havene absorberer 93% av energien som samles opp på jorda. Ved økt temperatur ekspanderer vannet.'
lot: 3
num: 17
---

Økning i vanntemperatur
