---
title: 'Wzrost temperatury wody'
backDescription: 'Oceany absorbują 91% energii, która gromadzi się na powierzchni Ziemi. Woda, ogrzewając się, zwiększa swoją objętość.'
lot: 3
num: 17
---
