---
title: 'Deshielo de la banquisa'
backDescription: 'El deshielo de la banquisa (hielo marino) no es la causa de la subida del nivel del mar (un cubito de hielo que se derrite en un vaso, no hace que el agua desborde el vaso). Sin embargo, este deshielo deja paso a zonas más oscuras (el mar) que absorben má'
lot: 1
num: 18
---
