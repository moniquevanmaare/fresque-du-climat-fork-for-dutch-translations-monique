---
title: 'Fonte de la banquise'
backDescription: "La fonte de la banquise n'est pas responsable de la montée des eaux (un glaçon qui fond dans du pastis ne fait pas déborder le verre)."
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_18_fonte_de_la_banquise'
youtubeCode: 'qtZ3J_HEHuA'
instagramCode: 'CKUB7UsoVbl'
lot: 1
num: 18
---

Le volume occupé par la glace sous la surface est exactement le même que celui de la glace une fois fondue. C'est le principe de la poussée d'Archimède. La banquise se forme lorsque l'eau de mer est à une température inférieure à -1.8°C pendant une longue période. La banquise est salée lors de sa formation, puis s'adoucit avec le temps. La banquise se distingue des calottes glaciaires par le fait que c'est une glace flottante. Les icebergs sont aussi de la glace flottante, mais viennent de l'avancée des calottes glaciaires, et qui sont de la glace d'eau douce. Les étendues de banquise sont principalement en arctique, et en périphérie du continent antarctique. Il faut distinguer la banquise saisonnière qui se forme en hiver et dégèle en été, de la banquise pérenne. Quand on parle de fonte de la banquise, cela fait référence à la banquise pérenne.
