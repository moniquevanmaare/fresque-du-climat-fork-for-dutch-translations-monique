---
title: 'Smelting av sjøis'
backDescription: 'Smelting av sjøis fører ikke til havnivåstigning (akkurat som en smeltende isbit ikke får et glass til å renne over). Men når isen smelter, blir overflaten erstattet med det mørke havet, som absorberer flere solstråler enn hvit is.'
lot: 1
num: 18
---
