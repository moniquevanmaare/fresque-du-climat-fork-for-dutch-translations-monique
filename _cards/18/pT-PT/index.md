---
title: 'Derretimento do Gelo Marítimo'
backDescription: 'O derretimento do gelo marítimo não é responsável pela elevação do nível do mar (um cubo de gelo que derrete num copo de água não faz a água transbordar). No entanto, ao derreter, dá lugar a uma superfície mais escura (o oceano), que absorve mais raios so'
lot: 1
num: 18
---
