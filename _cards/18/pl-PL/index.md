---
title: 'Topnienie morskiej pokrywy lodowej'
backDescription: 'Topnienie lodu morskiego nie wpływa na wzrost poziomu wód w morzach i oceanach (kostka lodu, rozpuszczając się w wypełnionej po brzegi szklance wody nie spowoduje przelania się zawartości). Jednak jego topnienie powoduje, że większa powierzchnia Ziemi jes'
lot: 1
num: 18
---
