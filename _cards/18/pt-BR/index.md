---
title: 'Derretimento da banquisa / gelo marinho'
backDescription: 'O derretimento do gelo marinho não é responsável pela elevação do nível do mar (um cubo de gelo que derrete em um copo de água não faz a água transbordar). Entretanto, ao derreter, aumenta a proporção da superfície escura nos oceanos, que absorvem maior q'
lot: 1
num: 18
---
