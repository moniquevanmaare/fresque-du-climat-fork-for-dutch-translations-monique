---
title: 'Deniz buzullarının erimesi'
backDescription: 'Deniz buzullarının erimesi, deniz seviyesinde yükselişe sebep olmamaktadır (bir bardak suyun içerisindeki buz küpü eridiğinde bardaktan su taşırmaz). Ancak deniz buzulları eridikçe, daha koyu renkte olan denizlerin alanı genişlemekte ve buzdan geri yansıy'
lot: 1
num: 18
---
