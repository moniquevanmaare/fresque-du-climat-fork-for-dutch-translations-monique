---
title: 'Smeltende ijskappen'
backDescription: 'Op Aarde bevinden zich twee grote ijskappen (of gletsjers), namelijk in Groenland en op Antarctica. Als de Antarctische en de Groenlandse ijskappen helemaal zouden smelten, zou de zeespiegel door Groenland met 7m en door Antarctica met 54m stijgen. Tijden'
lot: 3
num: 19
---
Deze illustraties laten zien wat de winst of het verlies aan massa is van de ijskappen, aangegeven in centimeters water per jaar (cm water/jaar) en gravimetrisch gemeten. In blauw de massawinst (omdat het meer sneeuwt) en in rood de verliezen (van gletsjers die sneller richting de oceaan stromen). De ijskappen zijn rond de 3000m diep.
