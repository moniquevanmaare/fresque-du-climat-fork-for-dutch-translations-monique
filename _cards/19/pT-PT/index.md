---
title: 'Derretimento das calotas polares'
backDescription: 'Os calotes glaciares situam-se na Gronelândia e na Antártida. Se derretessem por completo, provocariam um aumento nos níveis dos oceanos de 7 metros para a Gronelândia e 54 metros para a Antártida. Durante a última era glacial, os calotes eram tão grandes'
lot: 3
num: 19
---
