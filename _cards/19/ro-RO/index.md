---
title: 'Topirea calotelor de gheață'
backDescription: 'Ghețarii continentali (sau calotele de gheață) se află în Groenlanda și Antarctica. Dacă se vor topi complet, vor provoca o creștere a nivelului oceanului de 7 metri  (pentru Groenlanda) și 54 de metri (pentru Antarctica). În ultima eră glaciară, calotele'
lot: 3
num: 19
---
