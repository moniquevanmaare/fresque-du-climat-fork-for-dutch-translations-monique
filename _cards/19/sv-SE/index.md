---
title: 'Smältning av istäcken'
backDescription: 'Kontinentala glaciärer (eller istäcken) finns på Grönland och Antarktis. Om de skulle smälta helt och hållet, skulle de orsaka en höjning av havsnivån med 7 meter för Grönland, och 54 meter för Antarktis. Under den senaste istiden var istäckena så stora a'
lot: 3
num: 19
---
