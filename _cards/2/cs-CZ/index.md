---
title: 'Průmysl'
backDescription: 'Průmysl využívá fosilní paliva a elektřinu. Produkuje 40 % celkových emisí skleníkových plynů.'
lot: 2
num: 2
---
