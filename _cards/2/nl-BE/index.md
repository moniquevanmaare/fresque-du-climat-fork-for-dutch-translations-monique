---
lot: 2
num: 2
backDescription: >-
  De industrie gebruikt fossiele brandstoffen en elektriciteit en is
  verantwoordelijk voor 40% van de totale uitstoot van broeikasgassen.
title: Industrie
---
Dit betreft het produceren van alle consumentengoederen. De industrie bestaat uit veel verschillende sectoren. Voor wat betreft de uitstoot van broeikasgassen (BKG's) zijn de belangrijkste hiervan de papier, cement, staal, aluminium en chemische sectoren. Om emissies van de industrie te reduceren is het noodzakelijk  om de levensduur van producten te verlengen en consumptie te reduceren.
