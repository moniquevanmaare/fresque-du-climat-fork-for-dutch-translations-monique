---
title: "Forstyrrelse af vandets kredsløb"
backDescription: "Når havene og atmosfæren bliver varmere, medfører det øget fordampning. Mere vanddamp i luften giver flere skyer og mere nedbør i form af kraftige regnskyl. Samtidig medfører øget fordampning også tørke.
"
lot: 3
num: 20
---
