---
title: 'Forstyrrelser i vannets kretsløp'
backDescription: 'Fordampningen av vann fra havoverflaten øker ved økende vann- og lufttemperaturer. Dette fører til flere regnskyer og mer regn. Hvis temperaturøkningen skjer på land, tørker jorden ut.'
lot: 3
num: 20
---
