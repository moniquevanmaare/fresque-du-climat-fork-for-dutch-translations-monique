---
title: 'Aumento de la temperatura'
backDescription: 'Nos referimos aquí a la temperatura del aire a nivel del suelo, como promedio de la superficie terrestre. Ha aumentado 1°C desde 1900. Según los diferentes escenarios, este aumento será de entre 2°C y 5°C de aquí a 2100. Al final del último periodo glacia'
lot: 1
num: 21
---
