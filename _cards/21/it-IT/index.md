---
title: 'Aumento di temperatura'
backDescription: "Parliamo qui della temperatura dell'aria al suolo e in media sulla superficie della terra. E' già aumentata di 1,2°C dal 1900. Secondo gli scenari delle emissioni future, sarà aumentata da 1,5°C fino a 5°C da qui al 2100. Durante l'ultima era glaciale, la"
lot: 1
num: 21
---
