---
title: 'Anstieg des Meeresspiegels'
backDescription: 'Seit 1900 ist der Meeresspiegel um 20cm angestiegen. Grund dafür ist die thermische Ausdehnung des Ozeanwassers und das Abschmelzen von Gletschern und Eisschilden.'
lot: 1
num: 22
---
