---
title: 'Aumento del nivel del mar'
backDescription: 'Desde 1900, el nivel del océano ha subido 20 cm. Esto se debe a la dilatación del agua, al deshielo de los glaciares y al deshielo de los casquetes polares.'
lot: 1
num: 22
---
