---
title: 'Montée des eaux'
backDescription: "Depuis 1900, le niveau de l’océan a monté de 20 cm.Cela est dû à la dilatation de l'eau, la fonte des glaciers et la fonte des calottes."
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_22_mont%C3%A9e_des_eaux'
youtubeCode: 'iypRDzcMA-o'
instagramCode: 'CM2P9noInE8'
lot: 1
num: 22
---

Il est important de noter que les prévisions concernant la hausse du niveau de l'eau sont très conservatrices. Certains phénomènes, qui sont compris d'un point de vu qualitatif, mais pas quantitatif, ne sont tout simplement pas chiffrés dans le rapport du GIEC. C'est le cas des moulins, par exemple. Ce sont des passages entre l'eau des lacs glaciaires à la surface des calottes et le socle rocheux. Une fois que l'eau s'introduit dans ces passages, elle vient lubrifier le contact entre le socle rocheux et la calotte glaciaire, ce qui facilite la dérive des glaciers vers la mer. Les chiffres concernant la montée des eaux seront donc très probablement revus à la hausse dans les prochains rapports.
