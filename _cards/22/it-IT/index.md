---
title: "Innalzamento del livello dell'acqua"
backDescription: "Dal 1900, il livello dell'oceano si è alzato di 20 cm. Ciò è dovuto alla dilatazione dell'acqua, allo scioglimento dei ghiacciai e allo scioglimento delle calotte glaciali"
lot: 1
num: 22
---
