---
title: 'Havnivåstigning'
backDescription: 'Siden 1900 har havnivået steget med 20 cm. Havnivåstigning skyldes varmeutvidelse (termisk ekspansjon) av havvannet, og smelting av isbreer og innlandsis:'
lot: 1
num: 22
---
