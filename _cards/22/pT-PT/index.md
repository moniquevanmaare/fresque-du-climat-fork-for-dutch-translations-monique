---
title: 'Aumento do nível do mar'
backDescription: 'Desde 1900, o nível do mar subiu 20 cm. Esta subida é causada pela expansão térmica das águas oceânicas, pelo derretimento dos glaciares e pela fusão dos calotes polares.'
lot: 1
num: 22
---
