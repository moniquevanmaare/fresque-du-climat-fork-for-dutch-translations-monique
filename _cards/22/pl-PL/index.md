---
title: 'Wzrost poziomu mórz'
backDescription: 'Od 1900 r. poziom mórz i oceanów podniósł się o 20 cm. Doszło do tego na skutek wzrostu objętości wody, topnienia lodowców i topnienia lądolodów.'
lot: 1
num: 22
---
