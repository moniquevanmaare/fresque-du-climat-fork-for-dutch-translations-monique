---
title: 'aumento do nível do mar'
backDescription: 'Desde 1900, o nível do mar subiu 20 cm, devido à expansão térmica das águas oceânicas e ao derretimento das geleiras e das calotas polares'
lot: 1
num: 22
---
