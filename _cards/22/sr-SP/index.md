---
title: 'Porast nivoa mora'
backDescription: 'Od 1900. godine nivo mora je porastao 20 cm. Porast nivoa mora je posledica termalnog širenja voda okeana i otapanja glečera i kontinentalnih lednika.'
lot: 1
num: 22
---
