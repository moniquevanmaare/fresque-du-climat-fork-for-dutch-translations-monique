---
title: 'Havsnivåhöjning'
backDescription: 'Sedan 1900 havsnivån har stigit med 20 cm. Höjning av havsnivån orsakas av havsvattnets termiska expansion och smältningen av glaciärer.'
lot: 1
num: 22
---
