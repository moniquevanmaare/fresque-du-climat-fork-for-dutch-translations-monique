---
title: 'Hemmung der Kalkbildung'
backDescription: 'Sinkt der pH-Wert, wird die Bildung von Kalk (genauer gesagt von Kalkschalen) erschwert.'
lot: 4
num: 23
---
