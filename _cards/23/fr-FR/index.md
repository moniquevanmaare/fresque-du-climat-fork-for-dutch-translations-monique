---
title: 'Problèmes de calcification'
backDescription: 'Si le pH baisse, la formation de calcaire devient plus difficile, notamment pour les coquilles.'
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_23_probl%C3%A8mes_de_calcification'
youtubeCode: 'A3zQW_C7RMI'
instagramCode: ''
lot: 4
num: 23
---

La constitution du calcaire (calcification) est la réaction chimique suivante : Ca++ + 2HCO3- ⇔ CaCO3 + H2O + CO2. Elle nécessite la présence d'ions bicarbonate HCO3–. Or la quantité de ces ions dans l'eau dépend du pH : Dans l'eau, le dioxyde de carbone, l'acide carbonique, l'ion bicarbonate et l'ion carbonate sont en équilibre, en fonction du pH : CO2 + H2O ⇔ H2CO3 ⇔ H+ + HCO3– ⇔ 2 H+ + CO32–. L'ajout d'un acide déplace les équilibres vers la gauche. Dit autrement, si le pH baisse, les ions bicarbonates sont en moins grande quantité. D'où la difficulté de fabriquer du calcaire pour les organisme qui en ont besoin.
