---
title: 'Forhindret forkalkning'
backDescription: 'Når pH-verdien går ned, blir dannelsen av kalsiumkarbonat (og videre produksjonen av skjell) redusert.'
lot: 4
num: 23
---
