---
title: 'Onemogućavanje procesa kalcifikacije'
backDescription: 'Kada se pH vrednost smanji, formiranje kalcijum karbonata (preciznije, kalcifikovanih oklopa životinja) je otežano.'
lot: 4
num: 23
---
