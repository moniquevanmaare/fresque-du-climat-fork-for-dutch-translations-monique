---
title: 'Hindrad förkalkningsprocess'
backDescription: 'När pH-värdet sjunker blir förkalkning svårare (speciellt bildandet av kalkhaltiga skal).'
lot: 4
num: 23
---
