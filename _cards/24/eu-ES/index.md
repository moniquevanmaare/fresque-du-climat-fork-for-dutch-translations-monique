---
title: 'Ozeanoaren azidotzea'
backDescription: 'CO₂-a ozeanoan disolbatzen denean, azido karboniko bilakatzen da (H₂CO₃ eta ondotik HCO₃). Ozeanoa azidoagoa bilakatzen da (pH-a apaltzen da).'
lot: 2
num: 24
---
