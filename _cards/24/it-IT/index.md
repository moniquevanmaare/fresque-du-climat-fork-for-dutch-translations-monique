---
title: "L'acidificazione dell'oceano"
backDescription: "Quando il CO2 si discioglie nell'oceano, si trasforma in H2CO3 (acido carbonico) e poi HCO3 (bicarbonato). Questo ha l'effetto di acidificare l'oceano (il pH diminuisce)."
lot: 2
num: 24
---
