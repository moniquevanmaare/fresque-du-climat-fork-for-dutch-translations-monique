---
title: "Acidificarea oceanelor"
backDescription: "Când CO2 se dizolvă în ocean, acesta se transformă în H2CO3 (acid carbonic) și apoi în HCO3 (bicarbonat). Aceastea au ca efect acidificarea oceanului (pH-ul
scade)."
lot: 2
num: 24
---
