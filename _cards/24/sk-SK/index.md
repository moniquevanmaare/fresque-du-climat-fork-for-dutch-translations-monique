---
title: 'Okysľovanie oceánov'
backDescription: 'Keď sa CO2 rozpustí v oceáne, premení sa na kyslé ióny (H2CO3 and HCO3-). To má za následok okysľovanie oceánu (pH klesá).'
lot: 2
num: 24
---
