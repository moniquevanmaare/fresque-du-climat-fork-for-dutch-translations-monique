---
title: 'Acidifikacija okeana'
backDescription: 'Kada se CO2 rastopi u okeanu, pretvara se u kisele jone (H2CO3 i HCO3-). Ova transformacija kao posledicu ostavlja acidifikaciju okeana (pH vrednost opada).'
lot: 2
num: 24
---
