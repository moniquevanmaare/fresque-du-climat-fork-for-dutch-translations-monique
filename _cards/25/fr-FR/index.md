---
title: 'Biodiversité terrestre'
backDescription: "Les animaux et les plantes sont affectés par les changements de température et du cycle de l'eau : ils se déplacent ou disparaissent (ou, plus rarement, ils prolifèrent)."
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_25_biodiversit%C3%A9_terrestre'
youtubeCode: 'TP8Tu4Gq2CE'
instagramCode: ''
lot: 4
num: 25
---

Aujourd'hui, la biodiversité terrestre est avant tout mise à mal par d'autres facteurs que le climat : déforestation, disparition des habitats naturels, pesticides, pollutions diverses. Il n'empêche que le changement climatique va largement contribuer à la disparition des espèces dans les décennies à venir.
