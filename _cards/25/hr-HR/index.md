---
title: 'Kopnena bioraznolikost'
backDescription: 'Promjene temperature i poremećaji hidrološkog ciklusa utječu na životinje i biljke. Prisiljene su na migraciju ili zbog njih izumiru. Pojedine vrste, pak, mogu ojačati i razmnožiti se.'
lot: 4
num: 25
---
