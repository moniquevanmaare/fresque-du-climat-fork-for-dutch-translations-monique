---
title: 'Szárazföldi biodiverzitás'
backDescription: 'A hőmérséklet-változás és a vízkörforgás zavara hatással van a növény- és állatvilágra. Egyes fajokat migrációra késztethet, a kihalásukat okozhatja vagy ritka esetben fokozhatja a szaporodásukat.'
lot: 4
num: 25
---
