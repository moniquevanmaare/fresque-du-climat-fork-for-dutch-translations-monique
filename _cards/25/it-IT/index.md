---
title: 'Biodiversità terrestre'
backDescription: 'Gli animali e le piante sono influenzati dalle variazioni della temperatura e da quelle del ciclo idrico: si spostano o scompaiono (o, più raramente, proliferano).'
lot: 4
num: 25
---
