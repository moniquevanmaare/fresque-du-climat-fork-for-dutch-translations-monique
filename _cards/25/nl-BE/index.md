---
title: 'Terrestriële biodiversiteit'
backDescription: "Veranderingen van de temperatuur en van de waterkringloop hebben grote gevolgen voor planten en dieren. Veel soorten hebben als reactie op de klimaatverandering hun verspreidingsgebied, seizoensactiviteiten, migratieschema's, omvang en/of interactie met a"
lot: 4
num: 25
---
De biodiversiteit op aarde wordt op dit moment vooral bedreigd door andere factoren dan het klimaat, bijvoorbeeld door ontbossing, het verdwijnen van natuurlijke leefomgevingen, het gebruik van pesticides en andere vormen van vervuiling. Maar klimaatverandering zal in de komende decennia ook een grote bijdrage leveren aan het verdwijnen van soorten.
