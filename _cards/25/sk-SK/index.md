---
title: 'Suchozemská biodiverzita'
backDescription: 'Zvieratá a rastliny sú ovplyvnené zmenami teplôt a vodného cyklu: migrujú, vyhýnajú alebo sa veľmi zriedka ich počet aj zväčší.'
lot: 4
num: 25
---
