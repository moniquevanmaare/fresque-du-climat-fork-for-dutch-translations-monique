---
title: 'Landlevande biologisk mångfald'
backDescription: 'Djur och växter påverkas av både temperaturförändringar och störningar av vattnets kretslopp. De migrerar, dör, eller (mer sällan) sprider sig.'
lot: 4
num: 25
---
