---
title: 'Esondazioni'
backDescription: "Un ciclo idrico perturbato puo' portare a scarsità o abbondanza d'acqua. Più acqua puo' generare esondazioni. Se il suolo è stato precedentemente inaridito dalla siccità, la situazione è peggiore perché l'acqua scorre in superficie e non viene assorbita."
lot: 4
num: 26
---
