---
title: 'Inundações'
backDescription: 'A perturbação do ciclo da água pode ocasionar mais ou menos água. Mais água, pode provocar inundações. A gravidade da situação é maior se o solo estiver endurecido pela seca, pois a água escorre.'
lot: 4
num: 26
---
