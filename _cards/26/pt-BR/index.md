---
title: 'Enchentes'
backDescription: 'A perturbação do ciclo da água pode ocasionar mais ou menos água. Mais água pode levar à inundações nas terras.  A situação se agrava se o solo estiver seco porque a água não escoa.'
lot: 4
num: 26
---
