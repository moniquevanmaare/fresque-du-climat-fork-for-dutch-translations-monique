---
title: 'Översvämningar'
backDescription: 'Störningar av vattnets kretslopp kan leda till ökade eller minskade mängder vatten. Mer vatten kan leda till översvämningar av floder och vattendrag. Om jordskorpan är uttorkad kan det få värre konsekvenser eftersom vattnet rinner av, i stället för att ab'
lot: 4
num: 26
---
