---
title: 'Marine Biodiversity'
backDescription: 'Pteropods and coccolithophores are at the base of the ocean food chain. Therefore, if they disappear, all marine biodiversity is threatened. The warming of ocean waters also threatens marine biodiversity.'
lot: 4
num: 27
---
