---
title: 'Biodiversidade Marinha'
backDescription: 'Os pterópodes e os cocolitóforos estão na base da cadeia alimentar, pelo que o seu desaparecimento ameaça toda a biodiversidade marinha. O aquecimento da água desempenha também um papel importante no enfraquecimento da biodiversidade marinha.'
lot: 4
num: 27
---
