---
title: 'Marin biologisk mångfald'
backDescription: 'Rovsimsnäckor och Coccoliter utgör basen av havets näringskedja. Därför hotas hela den biologiska mångfalden i havet om de försvinner. Uppvärmningen av havet är även ett direkt hot mot den marina biologiska mångfalden.'
lot: 4
num: 27
---
