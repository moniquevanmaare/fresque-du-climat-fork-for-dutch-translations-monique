---
title: 'Přenos nemocí'
backDescription: 'Kvůli globálnímu oteplování zvířata migrují. Některé z nich přenášejí nemoci a mohou se dostat do oblastí, kde není populace vůči těmto nemocem imunní.'
lot: 5
num: 28
---
