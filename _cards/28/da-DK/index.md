---
title: 'Spredning af sygdomme'
backDescription: 'Ændringerne i klima fører til at dyrearter spreder sig på nye måder. Nogle af dyrene kan bære sygdomme med sig, som siden skaber store problemer i områder, hvor befolkningen ikke er immune overfor dem.'
lot: 5
num: 28
---
