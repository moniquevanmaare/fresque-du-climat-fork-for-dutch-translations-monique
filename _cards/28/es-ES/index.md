---
title: 'Vectores de enfermedades'
backDescription: 'Con el calentamiento global, los animales migran. Algunos de ellos son vectores de enfermedades y llegan a zonas donde la población no es inmune a dichas enfermedades.'
lot: 5
num: 28
---
