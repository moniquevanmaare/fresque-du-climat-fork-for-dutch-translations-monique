---
title: 'Sykdomsvektorer'
backDescription: 'Dyr migrerer på grunn av global oppvarming. Noen av dem er sykdomsvektorer og kan nå områder hvor befolkningen ikke er immun mot disse sykdommene.'
lot: 5
num: 28
---
