---
title: 'Vectoren voor ziekteoverdracht'
backDescription: 'Onder invloed van klimaatverandering zullen diersoorten migreren naar nieuwe gebieden; sommige soorten dragen ziektes waartegen de inheemse populaties minder of geen weerstand hebben.'
lot: 5
num: 28
---
Het probleem hier is niet zozeer het voortbestaan van vectorziekten als wel de migratie ervan naar andere gebieden. Deze kaart komt idealiter na kaart 25 omdat vectorziekten een onderdeel uitmaken van biodiversiteit.
