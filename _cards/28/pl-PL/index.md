---
title: 'Nosiciele chorób'
backDescription: 'Z powodu ocieplenia zwierzęta migrują. Niektóre z nich przenoszą choroby, na które zamieszkałe w innych strefach organizmy nie są uodpornione.'
lot: 5
num: 28
---
