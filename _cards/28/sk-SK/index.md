---
title: 'Prenášači chorôb'
backDescription: 'Globálne otepľovanie prináša migráciu zvierat. Niektoré prenášajú choroby a môžu dosiahnuť oblasti, v ktorých populácia nie je imunizovaná proti týmto chorobám.'
lot: 5
num: 28
---
