---
title: 'Sjukdomsvektorer'
backDescription: 'Den globala uppvärmningen orsakar migration av djurarter. Vissa djur är sjukdomsbärare, och de kommer att nå områden där befolkningen inte är motståndskraftig mot av dessa sjukdomar.'
lot: 5
num: 28
---
