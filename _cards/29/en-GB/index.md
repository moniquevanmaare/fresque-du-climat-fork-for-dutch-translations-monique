---
title: 'Pteropods and Coccolithophores'
backDescription: 'Pteropods are a type of zooplankton and coccolithophores a type of phytoplankton. These organisms have a calcified shell.'
wikiUrl: 'https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_29_pteropods_and_coccolithophores'
youtubeCode: '6gHERqKJc2A'
instagramCode: ''
lot: 4
num: 29
---

Lots of jokes about coccolithophores because the name is hard to say.
