---
title: 'Pteropodoak eta kokolitoforoak'
backDescription: 'Pteropodoak zooplanktona dira eta kokolitoforoak fitoplanktona. Mikroorganismo horiek kareharrizko maskorra dute.'
lot: 4
num: 29
---
