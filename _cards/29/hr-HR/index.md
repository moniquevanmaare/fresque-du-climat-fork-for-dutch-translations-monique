---
title: 'Pteropodi i kokolitofori'
backDescription: 'Pteropodi su vrsta zooplanktona, a kokolitofori vrsta fitoplanktona. Njihove su ljušture građene su od kalcijevog karbonata.'
lot: 4
num: 29
---
