---
title: 'Pteropodi e coccolitofori'
backDescription: 'Gli pteropodi sono zooplancton e i coccolitofori fitoplancton. Questi microrganismi hanno un guscio di calcare.'
lot: 4
num: 29
---
