---
title: 'Pteropoder og kalkflagellater'
backDescription: 'Pteropoder er en type dyreplankton og kalkflagellater er en type planteplankton. Disse mikroorganismene har et forkalket skall.'
lot: 4
num: 29
---
