---
title: 'Pteropoda en coccolithoforen'
backDescription: 'Pteropoda zijn zoöplankton en coccolithophoren zijn fytoplankton. Deze organismen hebben een kalkhoudende schaal.'
lot: 4
num: 29
---
Deze kaart leent zich voor grappen omdat met name "coccolithoforen" lastig uit te spreken is ;)
