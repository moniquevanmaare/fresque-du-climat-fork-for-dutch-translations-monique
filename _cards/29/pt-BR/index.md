---
title: 'pterópodes e cococolitóforos'
backDescription: 'Os pterópodes são zooplânctons e os cococolitóforos são fitoplânctons. Esses microrganismos possuem uma concha calcária.'
lot: 4
num: 29
---
