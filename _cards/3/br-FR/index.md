---
backDescription: L’utilisation du bâtiment (logement et services) utilise des énergies
    fossiles et de l'électricité. Cela représente 20% des Gaz à Effet de Serre (GES).
instagramCode: ''
lot: 2
num: 3
title: Implij ar savadurioù
wikiUrl: https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_3_utilisation_des_b%C3%A2timents
youtubeCode: rxHxR0Ald3I
---
