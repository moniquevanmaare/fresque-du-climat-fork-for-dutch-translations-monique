---
title: 'Užívání staveb'
backDescription: 'Užívání staveb (pro bydlení a komerční využití) spotřebovává fosilní paliva a elektřinu. Produkuje 20 % celkových emisí skleníkových plynů.'
lot: 2
num: 3
---
