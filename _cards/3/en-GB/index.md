---
title: 'Building Usage'
backDescription: 'The building sector (housing and commercial use) uses fossil fuels and electricity. It accounts for 20% of greenhouse gas (GHG) emissions.'
wikiUrl: 'https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_3_building'
youtubeCode: 'csAunDBa8vY'
instagramCode: ''
lot: 2
num: 3
---

We consider here buildings' usage, not their construction (which is included in the card on Industry). This includes heating, air-conditioning, lighting, electronics, etc. The big topic in Europe and the US is the thermal insulation of buildings. As far as new construction is concerned, it is vital to build well-insulated buildings. However, the stakes are limited as the standards for new buildings are much higher than in the past and only a small proportion (1%) of buildings are constructed each year. The challenge is therefore much more in the thermal renovation of buildings.
