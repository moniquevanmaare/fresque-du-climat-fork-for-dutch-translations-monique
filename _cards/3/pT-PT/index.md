---
title: 'Utilização dos edifícios'
backDescription: 'O setor da construção civil (habitação e uso comercial) utiliza combustíveis fósseis e eletricidade. Representa 20% das emissões de gases de efeito estufa (GEE).'
lot: 2
num: 3
---
