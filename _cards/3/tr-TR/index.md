---
title: 'Binaların Kullanımı'
backDescription: "Yapı endüstrisi (mesken veya iş amaçlı) fosil yakıt ve elektrik kullanır. Sera etkisi oluşturan gazların %20'si yapı endüstrisi kaynaklıdır."
lot: 2
num: 3
---
