---
backDescription: La perturbation du cycle de l'eau peut amener plus ou moins d'eau.
    Moins d'eau, c'est une sécheresse. On estime ainsi que les sécheresses pourraient
    se multiplier à l’avenir.
instagramCode: ''
lot: 4
num: 30
title: Sec'horioù
wikiUrl: https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_30_s%C3%A9cheresses
youtubeCode: EX82KQmQ3yg
---
