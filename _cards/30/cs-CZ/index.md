---
title: 'Sucho'
backDescription: 'Narušení koloběhu vody může znamenat jak více vody, tak méně vody. Méně vody znamená sucho. V budoucnosti bude sucho pravděpodobně častějším jevem.'
lot: 4
num: 30
---
