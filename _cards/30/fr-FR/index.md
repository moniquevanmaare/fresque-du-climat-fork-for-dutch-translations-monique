---
title: 'Sécheresses'
backDescription: "La perturbation du cycle de l'eau peut amener plus ou moins d'eau. Moins d'eau, c'est une sécheresse. On estime ainsi que les sécheresses pourraient se multiplier à l’avenir."
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_30_s%C3%A9cheresses'
youtubeCode: 'EX82KQmQ3yg'
instagramCode: ''
lot: 4
num: 30
---

Une sécheresse est une période de temps anormalement sec suffisamment longue pour causer un grave déséquilibre hydrologique. On peut parler de sécheresse agricole si l'on mesure l'humidité du sol ou de sécheresse météorologique dans le cas d'une baisse de précipitation. Une mégasécheresse est une sécheresse persistante et étendue, d’une durée très supérieure à la normale (en général, une décennie ou plus). Le manque de pluie et l'évaporation sur le sol sont les causes des sécheresses. Sur la carte du monde représentée par régions (hexagones) (issue 6è rapport de synthèse du GIEC), les zones vertes subissent une diminution des sécheresses, les zones orange une augmentation. Le nombre de points représente le degré de confiance dans la contribution humaine au changement observé : ... = Haut, .. = Moyen, . = Faible. Impact et niveau de confiance par région du monde.
