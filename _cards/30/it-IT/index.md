---
title: 'Siccità'
backDescription: "Un ciclo idrico pertubato puo' portare a scarsità o abbondanza d'acqua. Meno acqua provoca la siccità. Si prevede che le siccità potrebbero moltiplicarsi in futuro."
lot: 4
num: 30
---
