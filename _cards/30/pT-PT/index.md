---
title: 'Secas'
backDescription: 'A perturbação do ciclo da água pode ocasionar mais ou menos água. Quando esses distúrbios provocam diminuição da quantidade de água, verfica-se uma seca. Estima-se que as secas serão mais frequentes no futuro.'
lot: 4
num: 30
---
