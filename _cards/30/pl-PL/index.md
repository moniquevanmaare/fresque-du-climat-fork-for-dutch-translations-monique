---
title: 'Susze'
backDescription: 'Zaburzenia obiegu wody w przyrodzie mogą wpływać na wzrost lub spadek poziomu wód. Niedobór wody powoduje susze. Przewiduje się, że w przyszłości susze będą występować coraz częściej.'
lot: 4
num: 30
---
