---
title: 'Suchá'
backDescription: 'Narušenie vodného cyklu spôsobuje nárast alebo úbytok vody. Úbytok vody spôsobuje suchá. Odhaduje sa, že sa tieto suchá budú znásobovať v budúcnosti.'
lot: 4
num: 30
---
