---
title: 'Zoetwaterbronnen'
backDescription: 'Zoetwaterbronnen worden beïnvloed door veranderingen in de regenval en door het verdwijnen van gletsjers. Die spelen een regulerende rol in de waterstromen.'
lot: 5
num: 31
---
Het belangrijkste probleem hier is de verdwijning van de gletsjers. Gletsjers functioneren als reservoirs van zoet water in vaste vorm. Als ze smelten kan dit water stroomafwaarts als irrigatie voor gewassen gebruikt worden.
