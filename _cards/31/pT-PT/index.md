---
title: 'Reservas de Água Doce'
backDescription: 'As reservas de água doce são afetadas por mudanças na precipitação e pelo desaparecimento dos glaciares, que desempenham um papel regulador no fluxo dos cursos de água.'
lot: 5
num: 31
---
