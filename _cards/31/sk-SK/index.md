---
title: 'Sladkovodné zdroje'
backDescription: 'Sladkovodné zdroje sú ovplyvnené zmenami zrážok a miznutím ľadovcov, ktoré hrajú úlohu regulátora vodných tokov.'
lot: 5
num: 31
---
