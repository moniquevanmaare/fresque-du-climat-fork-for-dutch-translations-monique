---
title: 'Resursi slatke vode'
backDescription: 'Promene količina kiša i nestanak glečera koji regulišu tokove reka utiču na resurse slatke vode.'
lot: 5
num: 31
---
