---
title: 'Sötvattentillgångar'
backDescription: 'Sötvattentillgångarna påverkas av förändrad nederbörd och försvinnandet av glaciärer, vilka spelar en reglerande roll för flödet i floder och vattendrag.'
lot: 5
num: 31
---
