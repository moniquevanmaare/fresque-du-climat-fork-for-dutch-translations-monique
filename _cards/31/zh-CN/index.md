---
title: '淡水资源'
backDescription: ' 淡水资源受到降水量变化以及冰川消融的影响，因为这两者有着调节水流量的作用。'
lot: 5
num: 31
---
