---
title: 'Rückgang der Ernteerträge'
backDescription: 'Unsere Agrarproduktion kann durch Temperaturwandel, Dürren, extreme Wetterereignisse, Hochwasser und Überschwemmungen (z.B.: das Nildelta) beeinträchtigt werden.'
lot: 5
num: 32
---
