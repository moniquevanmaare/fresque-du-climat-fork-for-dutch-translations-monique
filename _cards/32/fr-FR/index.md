---
title: 'Baisse des rendements agricoles'
backDescription: 'La production agricole peut être affectée par la température, les sécheresses, les évènements extrêmes, les inondations et les submersions (ex : delta du Nil).'
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_32_baisse_rendements_agricoles'
youtubeCode: 'nP2YWbNHTsM'
instagramCode: ''
lot: 5
num: 32
---

Voici l'une des plus grosses menaces pour l'humanité. Ce sont déjà des baisses de rendements agricoles qui ont amené à des conflits au Rwanda et en Syrie.
