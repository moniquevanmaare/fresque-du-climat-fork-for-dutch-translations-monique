---
title: 'Calo delle rese agricole'
backDescription: 'La produzione agricola può essere influenzata dalla temperatura, la siccità, gli eventi estremi, le esondazioni e le sommersioni (ad esempio: Delta del Nilo).'
lot: 5
num: 32
---
