---
title: 'Minskade skörder'
backDescription: 'Matproduktion kan påverkas av temperaturförändringar, torka, extremväder, översvämningar eller kustöversvämningar (t ex i Nildeltat).'
lot: 5
num: 32
---
