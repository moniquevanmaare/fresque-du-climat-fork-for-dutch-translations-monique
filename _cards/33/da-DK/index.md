---
title: 'Oversvømmelser ved kysten'
backDescription: 'Cykloner og ekstremt vejr medfører mere vind og bølger, samt hyppigere lavtryk ved havoverfladen. Når lufttrykket falder med 1 hectopascal, stiger vandstanden med 1 cm. Derfor kan cykloner medføre flere oversvømmelser ved kysterne, som forstærker den havv'
lot: 4
num: 33
---
