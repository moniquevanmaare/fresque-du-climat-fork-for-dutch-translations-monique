---
title: 'Flom og stormflo ved kysten'
backDescription: 'Sykloner og værforstyrrelser gir vind (derfor bølger) og lavtrykksforhold. En endring i lufttrykket på en hectopascal vil føre til en endring i vannstanden på om lag en cm. Derfor kan sykloner forårsake stormflo, forsterket av havnivåstigning som allerede'
lot: 4
num: 33
---
