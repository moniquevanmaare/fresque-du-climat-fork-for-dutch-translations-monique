---
title: 'Overstromingen door de stijging van de zeespiegel'
backDescription: 'Cyclonen en weerstoringen betekenen veel wind (en golven) en lage luchtdruk, wat invloed heeft op de zeespiegel. Eén hectopascal minder veroorzaakt een zeespiegelstijging van één centimeter. Samen met de stijging door opwarming van de aarde versterkt dit'
lot: 4
num: 33
---
Niet te verwarren met rivieroverstromingen. Kustoverstromingen komen door hoog zeewater. Deze stijging kan exceptioneel zijn door extreme weersomstandigheden, of permanent, door het stijgen van de zeespiegel. 
