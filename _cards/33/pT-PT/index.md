---
title: 'Submersão Marinha'
backDescription: 'Ciclones e perturbações climáticas envolvem ventos (e, portanto, ondas) e baixas pressões. 1 hectopascal a menos representa um aumento do nível do mar de 1 cm. Por conseguinte, os ciclones podem causar submersão marinha (inundação costeira) amplificada pe'
lot: 4
num: 33
---
