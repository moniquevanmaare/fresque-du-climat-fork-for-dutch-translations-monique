---
title: 'Submersões'
backDescription: 'Ciclones e perturbações climáticas trazem vento (portanto ondas) e zonas de baixa pressão. Agora, 1 hectopascal a menos representa 1 cm a mais para o nível do mar. É por isso que eles podem causar submersões (grandes inundações nas costas) que se agravam'
lot: 4
num: 33
---
