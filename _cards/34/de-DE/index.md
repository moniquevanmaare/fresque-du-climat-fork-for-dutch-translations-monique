---
title: 'Zyklone'
backDescription: 'Zyklone ziehen ihre Energie aus dem warmen Wasser an der Meeresoberfläche. Durch den Klimawandel werden sie zunehmend stärker.'
lot: 4
num: 34
---
