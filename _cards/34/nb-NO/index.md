---
title: 'Sykloner'
backDescription: 'Sykloner bruker energi fra varmt vann på havets overflate. De blir sterkere på grunn av global oppvarming.'
lot: 4
num: 34
---
