---
title: 'Cykloner'
backDescription: 'Cykloner använder sig av energi från varmt vatten på havsytan. De blir allt starkare på grund av den globala uppvärmningen.'
lot: 4
num: 34
---
