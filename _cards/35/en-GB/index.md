---
title: 'Forest Fires'
backDescription: 'Forest fires start and spread more easily during droughts and heatwaves.'
wikiUrl: 'https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_35_forest_fires'
youtubeCode: '0hkG0xUIHp8'
instagramCode: ''
lot: 5
num: 35
---
