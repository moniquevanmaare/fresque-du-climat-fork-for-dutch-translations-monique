---
title: 'Incendies'
backDescription: 'Les incendies sont facilités par les sécheresses et les canicules.'
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_35_incendies'
youtubeCode: 's6uVJt_Mqdc'
instagramCode: ''
lot: 5
num: 35
---
