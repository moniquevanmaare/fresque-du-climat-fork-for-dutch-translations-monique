---
backDescription: Une manifestation de l’augmentation de température est la multiplication
    des canicules.
instagramCode: ''
lot: 5
num: 36
title: Mareadoù gor
wikiUrl: https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_36_canicules
youtubeCode: hrfVmTJom30
---
