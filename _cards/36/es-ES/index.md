---
title: 'Canículas'
backDescription: 'La multiplicación de las canículas (olas de calor) es una manifestación del aumento de la temperatura.'
lot: 5
num: 36
---
