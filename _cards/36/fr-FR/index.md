---
title: 'Canicules'
backDescription: 'Une manifestation de l’augmentation de température est la multiplication des canicules.'
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_36_canicules'
youtubeCode: 'hrfVmTJom30'
instagramCode: ''
lot: 5
num: 36
---

Une canicule, ou vague de chaleur, est un phénomène météorologique de températures de l'air anormalement fortes, diurnes et nocturnes, se prolongeant de quelques jours à quelques semaines, dans une zone relativement étendue. Sur la carte du monde représentée par régions (hexagones) (issue 6è rapport de synthèse du GIEC), les zones roses subissent une augmentation des extrêmes de chaleur. Le nombre de points représente le degré de confiance dans la contribution humaine au changement observé : ... = Haut, .. = Moyen, . = Faible. Impact et niveau de confiance par région du monde.
