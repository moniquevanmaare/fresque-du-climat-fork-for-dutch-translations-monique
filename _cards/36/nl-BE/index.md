---
title: 'Hittegolven'
backDescription: 'Door de opwarming van de aarde stijgt de kans op langdurige hittegolven'
lot: 5
num: 36
---
Een hittegolf is een meteorologisch fenomeen met abnormaal hoge luchttemperaturen, zowel overdag als 's nachts. Ze duren een paar dagen tot een paar weken, in een relatief groot gebied. Op de wereldkaart (de hexagonale vlakken) uit het 6e rapport van de IPCC hebben de regio's met rode vlakken te maken met een toename van (extreme) hitte. Het aantal puntjes vertegenwoordigt de mate van betrouwbaarheid dat de getoonde verandering het resultaat is van menselijke activiteit.
