---
title: 'Hambrunas'
backDescription: 'Las hambrunas pueden ocurrir por la disminución del rendimiento agrícola y por la reducción de la biodiversidad marina.'
lot: 5
num: 37
---
