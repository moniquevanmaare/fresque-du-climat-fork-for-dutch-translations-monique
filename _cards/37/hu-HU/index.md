---
title: 'Éhínségek'
backDescription: 'Az éhínséget okozhatja az alacsonyabb mezőgazdasági terméshozam és a tengeri biodiverzitás csökkenése.'
lot: 5
num: 37
---
