---
title: 'Fome'
backDescription: 'A fome pode ser causada pelo declínio dos rendimentos agrícolas ou pela redução da biodiversidade marinha.'
lot: 5
num: 37
---
