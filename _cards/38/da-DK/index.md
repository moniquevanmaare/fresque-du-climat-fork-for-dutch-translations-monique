---
title: 'Sundhed'
backDescription: 'Hungersnød, spredning af sygdomme, hedebølger og væbnet konflikt vil alle påvirke den menneskelige sundhed negativt.'
lot: 5
num: 38
---
