---
title: 'Ανθρώπινη υγεία'
backDescription: 'Λιμοί, μετάδοση νοσημάτων λόγω μετανάστευσης των ζωών-φορέων ασθενειών, καύσωνες και ένοπλες συρράξεις μπορούν να επηρεάσουν την ανθρώπινη υγεία.'
lot: 5
num: 38
---
