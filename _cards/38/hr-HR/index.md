---
title: 'Ljudsko zdravlje'
backDescription: 'Glad, novi vektori zaraznih bolesti, toplinski valovi i oružani sukobi mogu imati loše posljedice za ljudsko zdravlje.'
lot: 5
num: 38
---
