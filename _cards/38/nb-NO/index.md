---
title: 'Helse'
backDescription: 'Hungersnød, endring av smittekilder, hetebølger og væpnede konflikter kan påvirke folks helse.'
lot: 5
num: 38
---
