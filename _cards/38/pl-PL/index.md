---
title: 'Zdrowie człowieka'
backDescription: 'Na zdrowie ludzkie wpływają głód, migracje nosicieli chorób, upały i konflikty zbrojne.'
lot: 5
num: 38
---
