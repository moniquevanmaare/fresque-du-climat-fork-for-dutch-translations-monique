---
backDescription: Imaginez que vous vivez dans un endroit qui est miraculeusement épargné
    par le changement climatique. Quelques milliards d'humains risquent d'avoir très
    envie de le partager avec vous !!!
instagramCode: ''
lot: 5
num: 39
title: Repuidi klimatek
wikiUrl: https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_39_r%C3%A9fugi%C3%A9s_climatiques
youtubeCode: He2tKBiumfU
---
