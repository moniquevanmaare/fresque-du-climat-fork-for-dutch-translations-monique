---
title: 'Klímamenekültek'
backDescription: 'Képzeld el, hogy olyan helyen élsz, amit csoda folytán elkerült a klímaváltozás. Lehetséges, hogy több milliárd ember szeretne osztozni veled a lakóhelyeden.'
lot: 5
num: 39
---
