---
title: 'Klimaflyktninger'
backDescription: 'Tenk deg at du bor på et sted som mirakuløst har blitt spart fra klimaendringene. Flere milliarder mennesker vil kanskje dele dette stedet med deg.'
lot: 5
num: 39
---
