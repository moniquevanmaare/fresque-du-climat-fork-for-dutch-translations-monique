---
title: 'Refugiados climáticos'
backDescription: 'Imagine que vive num lugar que foi milagrosamente poupado pelas alterações climáticas. Vários biliões de seres humanos podem querer compartilhá-lo consigo.'
lot: 5
num: 39
---
