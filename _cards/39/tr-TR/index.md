---
title: 'İklim Mültecileri'
backDescription: 'İklim değişikliğinden mucizevi bir şekilde etkilenmemiş bir yerde yaşadığınızı düşünün. Milyarlarca insan sizinle bu yeri paylaşmak isteyebilir.'
lot: 5
num: 39
---
