---
title: 'Transportation'
backDescription: 'The transportation sector is highly dependent on petroleum. It accounts for 15% of greenhouse gas emissions.'
lot: 2
num: 4
---
