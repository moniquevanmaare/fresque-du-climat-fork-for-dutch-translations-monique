---
title: 'Transport'
backDescription: 'Le secteur du transport est très dépendant du pétrole. Il représente 15% des émissions de gaz à effet de serre.'
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_4_transport'
youtubeCode: 'oMxhN9RlMnA'
instagramCode: 'CLKGjM7ofxA'
lot: 2
num: 4
---

15%, c'est peu, mais cela varie beaucoup en fonction des pays et des modes de vie. Dans les pays occidentaux, la part du transport, et notamment celle de l'avion, peut représenter une très grosse partie de l'empreinte carbone de chacun. En fait, si vous prenez un ou quelques longs courriers dans l'année, cela constitue l'essentiel de votre bilan carbone.
