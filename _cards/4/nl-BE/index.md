---
title: 'De transportsector'
backDescription: 'De transportsector is zeer afhankelijk van aardolie en is verantwoordelijk voor 15% van de totale uitstoot van broeikasgassen.'
lot: 2
num: 4
---
15% lijkt misschien niet veel, maar het percentage uitstoot door transport varieert sterk per land en levensstijl. In Westerse landen is het aandeel van transport, vooral door luchtvaart, een belangrijk deel van de CO2-voetafdruk. Als je een of meer lange vluchten per jaar maakt, dan vormt dat het gros van je persoonlijke CO2-voetafdruk.
