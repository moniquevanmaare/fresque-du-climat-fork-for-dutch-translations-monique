---
title: 'Transporte'
backDescription: 'O setor de transporte é muito dependente do petróleo. Ele representa 15% das emissões de gases de efeito estufa (GEE).'
lot: 2
num: 4
---
