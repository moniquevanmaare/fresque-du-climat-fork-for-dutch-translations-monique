---
title: 'Permafrost'
backDescription: 'Permafrost is permanently frozen ground. It is starting to thaw, releasing into the atmosphere previously locked in methane and CO2 from decomposed biomass. This creates a positive feedback loop, just like forest fires and albedo changes due to melting se'
wikiUrl: 'https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_41_permafrost'
youtubeCode: '0eXsD4C15KQ'
instagramCode: 'CNaOV_FHH2D'
lot: 5
num: 41
---

Permafrost is soil that is permanently frozen for at least two consecutive years. The two last cards to be added to the Fresk after card 40 are potentially violent feedback loops or "climate bombs" which, if triggered, would cause us to lose control over the climate for good. Thermokarst are veritable bioreactors at the heart of the process of releasing frozen carbon: when the permafrost thaws, pieces of soil detach and fall into the water, bringing nutrients and carbon to the bacteria and plankton present in the sea, which degrade them into CO2 (in the water layers near the surface) and methane (CH4 -- in the oxygen-deprived depths).
