---
title: 'Permafrost'
backDescription: 'Le permafrost désigne le sol gelé en permanence. On constate qu’il commence à dégeler, relâchant dans l’air du méthane et du CO2, suite à la décomposition de la matière organique qui était jusque là gelée. Cela constitue une boucle de rétroaction positive'
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_41_permafrost'
youtubeCode: 'T_9vgHZkx90'
instagramCode: 'CNaMM10ocNX'
lot: 5
num: 41
---

Le permafrost, ou pergélisol en français, est un sol dont la température reste inférieure ou égale à 0°C pendant au moins deux années consécutives. Le Permafrost fait partie des boucles de rétroactions potentielles, des "bombes climatiques", qui, si elles se déclenchent, nous font perdre définitivement le contrôle sur le climat. Les thermokarst sont de véritables bioréacteurs au cœur du processus de relargage du carbone gelé : Lorsque le pergélisol dégèle, des morceaux de sol se détachent et tombent dans l’eau, apportant nutriments et carbone aux bactéries et au plancton présents dans la mer, qui les dégradent en CO2 (dans les couches d’eau proches de la surface) et en méthane ou CH4 (dans le fond privé d’oxygène).
