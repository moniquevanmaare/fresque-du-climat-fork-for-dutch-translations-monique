---
title: 'Permafrost'
backDescription: 'Permafrost je trajno zaleđeno tlo. No, počinje se topiti. Iz razgrađene biomase u permafrostu topljenjem se u atmosferu oslobađaju dotad zarobljeni metan i CO2. Njihovo oslobađanje uzrokuje pozitivnu povratnu spregu, jednako kao i šumski požari i albedo e'
lot: 5
num: 41
---
