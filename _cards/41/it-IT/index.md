---
title: 'Permafrost'
backDescription: "Il permafrost indica un suolo perennemente ghiacciato. Si osserva che esso sta cominciando a sciogliersi, rilasciando nell'aria metano e CO2 in seguito alla decomposizione della materia organica che fino ad ora è rimasta congelata. Se questo fenomeno dove"
lot: 5
num: 41
---
