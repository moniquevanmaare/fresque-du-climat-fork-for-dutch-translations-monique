---
title: 'Permafrost (pergelissolo)'
backDescription: 'Permafrost refere-se a solo permanentemente congelado. Verifica-se que está a começar a descongelar, libertando metano e CO2 no ar em resultado da decomposição de matéria orgânica previamente congelada. Isto constitui uma retroacção, da mesma forma que os'
lot: 5
num: 41
---
