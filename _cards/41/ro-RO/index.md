---
title: "Permafrost"
backDescription: "Permafrostul este stratul de sol care este permanent înghețat. S-a constatat că acesta începe să se dezghețe, eliberând în aer metanul care a fost depozitat în acest sol.
Dacă acest fenomen va fi accelerat, va exista un risc ridicat de instabilitate clima"
lot: 5
num: 41
---
