---
title: 'Metanhydrater'
backDescription: 'Metanhydrater er en form for frosne metandepoter, der ligger i eller under havbunden. Depoterne kan blive ustabile ved en stigning i havtemperaturer på mere end 2˚C, hvorved gassen smelter og frigives. Dette vil igen forstærke klimaforandringerne.'
lot: 5
num: 42
---
