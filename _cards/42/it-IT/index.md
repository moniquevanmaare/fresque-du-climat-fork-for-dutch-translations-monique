---
title: 'Rallentamento della corrente del Golfo'
backDescription: "La circolazione termoalina, di cui fa parte la corrente del Golfo, potrebbe rallentare a causa dell'afflusso di acqua dolce dovuto allo scioglimento della calotta glaciale della Groenlandia. Questo porterebbe ad un ulteriore squilibrio del ciclo dell'acqu"
lot: 5
num: 42
---
