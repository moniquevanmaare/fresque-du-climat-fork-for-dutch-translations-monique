---
title: 'Metanhydrat'
backDescription: 'Metanhydrat (eller metanklatrat) er en form for is som finnes på havbunnen langs kontinentale skråninger, som fanger metan. De kan bli ustabile over +2°C.  '
lot: 5
num: 42
---
