---
title: 'Methaanhydraten'
backDescription: 'Methaanhydraat, ook wel methaan-clathraat of methaanijs genoemd, is een vorm van waterijs dat grote hoeveelheden methaan bevat.
    Grote hoeveelheden clathraat komen voor in sedimenten op de bodem van de aardse oceanen. Als de temperatuur stijgt, worden dez'
lot: 5
num: 42
---
Thermohaliene circulatie, ook wel diepe oceaancirculatie genoemd, ontstaat door verschillen in de dichtheid van zeewater, waardoor zeestromingen ontstaan. Deze verschillen in dichtheid komen voort uit verschillen in temperatuur en zoutgehalte van het water, vandaar de term thermo – voor temperatuur – en halin – voor zoutgehalte. Zeewater heeft een hogere dichtheid als de temperatuur lager is en het zoutgehalte hoger. Wetenschappers gebruiken de term MOC  (Meridional Overturning Circulation). Wanneer het de Atlantische Oceaan betreft, spreken we van de AMOC (Atlantic Meridional Overturning Circulation). De Golfstroom is een oceaanstroom aan de oppervlakte die zijn oorsprong vindt tussen Florida en de Bahama's en verzwakt in de Atlantische Oceaan rond de lengtegraad van Groenland. Het is een van de componenten van AMOC.
