---
backDescription: Les énergies fossiles sont le charbon, le pétrole et le gaz. Elles
    sont utilisées principalement dans les bâtiments, le transport et dans l'industrie.
    Elles émettent du CO2 lors de la combustion.
instagramCode: ''
lot: 1
num: 5
title: Energiezhioù fosil
wikiUrl: https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_5_%C3%A9nergies_fossiles
youtubeCode: 2ONOXLLFzWQ
---
