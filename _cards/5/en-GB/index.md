---
title: 'Fossil Fuels'
backDescription: 'Fossil fuels are coal, oil and natural gas. They are used mainly in buildings, transportation and industry. They emit CO2 when burned.'
wikiUrl: 'https://wiki.climatefresk.org/en/index.php?title=En-en_adult_card_5_fossil_fuels'
youtubeCode: 'Cm6x4ikTUho'
instagramCode: ''
lot: 1
num: 5
---

There is often a debate between placing the fossil fuels cards before or after human activities. Like the chicken and the egg, there is no definitive answer. One should not waste time on this. The graph represents the emissions of fossil fuels. In a +2°C scenario, they should reach zero by 2070.
