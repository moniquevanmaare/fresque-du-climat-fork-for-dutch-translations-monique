---
title: 'Énergies fossiles'
backDescription: "Les énergies fossiles sont le charbon, le pétrole et le gaz. Elles sont utilisées principalement dans les bâtiments, le transport et dans l'industrie. Elles émettent du CO2 lors de la combustion."
wikiUrl: 'https://wiki.climatefresk.org/index.php?title=Fr-fr_adulte_carte_5_%C3%A9nergies_fossiles'
youtubeCode: '2ONOXLLFzWQ'
instagramCode: ''
lot: 1
num: 5
---

Il y a souvent un débat entre mettre les énergies fossiles avant ou après les activités humaines. C’est comme la poule et l’œuf : il n’y a pas de bonne réponse. C’est une question de dialectique. Il ne faut pas perdre de temps là-dessus. Le graphique représente les émissions mondiales de CO2 seul issues des énergies fossiles. La courbe en noir les émissions passées, et en couleur, les projections selon les 5 scenarii étudiés par le GIEC dans le 6è rapport (AR6). Pour les 2 scénarii bleus (SSP1-1.9 et SSP1-2.6), les émissions envisagées doivent devenir nulles, respectivement en 2060 et 2080. Les énergies fossiles sont : charbon, pétrole, gaz naturel.
