---
title: 'Fosilna goriva'
backDescription: 'Fosilna goriva su ugljen, nafta i zemni plin. Najviše se koriste u zgradama, prijevozu i industriji. Kada izgaraju oslobađa se CO2.'
lot: 1
num: 5
---
