---
title: 'Angovo fôsily'
backDescription: "Ny angovo fôsily dia ny aritany, ny solika fandrehitra ary  ny entona izay ilaina indrindra amin'ny trano, fitanterana ary amin'ny indostria. Izy ireo dia mamoaka CO2 rehefa ampiasaina."
lot: 1
num: 5
---
