---
title: 'Fossile brensler'
backDescription: 'Fossile brensler består av kull, olje, og naturgass. De brukes hovedsakelig i bygninger, transport og industri. De avgir CO2 når de brennes.'
lot: 1
num: 5
---
