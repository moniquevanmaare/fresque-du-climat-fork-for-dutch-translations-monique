---
title: 'Combustíveis Fósseis'
backDescription: 'Os combustíveis fósseis são o carvão, o petróleo e o gás natural. São usados principalmente em edifícios, transportes e indústria e emitem CO2 durante a combustão.'
lot: 1
num: 5
---
