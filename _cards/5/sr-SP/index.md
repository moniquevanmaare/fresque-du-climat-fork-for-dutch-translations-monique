---
title: 'Fosilna goriva'
backDescription: 'Fosilna goriva su ugalj, nafta i prirodni gas. Ona se uglavnom koriste u zgradama, u saobraćaju i industriji. Emituju CO2 prilikom sagorevanja.'
lot: 1
num: 5
---
