---
title: 'Entwaldung'
backDescription: 'Entwaldung ist das Fällen oder Verbrennen von Bäumen jenseits der Erneuerungskapazität des Waldes. 80 % der Abholzung steht im Zusammenhang mit Landwirtschaft.'
lot: 2
num: 6
---
