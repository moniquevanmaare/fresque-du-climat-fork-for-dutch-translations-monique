---
title: 'Deforestazioa'
backDescription: 'Deforestazioa, oihanaren berrosatze ahalmenetik haratago zuhaitzen moztea edo erretzea da. % 80an laborantzarekin lotua da.'
lot: 2
num: 6
---
