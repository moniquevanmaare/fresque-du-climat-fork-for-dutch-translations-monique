---
title: 'Desflorestação'
backDescription: 'A desflorestação consiste no corte ou queima de árvores para além da capacidade de renovação natural da floresta. 80% da desflorestação está relacionada com a agricultura.'
lot: 2
num: 6
---
