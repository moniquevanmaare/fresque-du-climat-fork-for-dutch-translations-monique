---
title: 'Wylesianie'
backDescription: 'Wylesianie oznacza wycinkę lub wypalanie drzew w tempie szybszym niż proces odnawiania się lasu. 80% wylesiania ma związek z rolnictwem.'
lot: 2
num: 6
---
