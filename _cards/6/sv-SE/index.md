---
title: 'Avskogning'
backDescription: 'Avskogning är definierat som att hugga ner eller bränna träd bortom skogens egna återhämtningsförmåga. 80% av avskogningen drivs av jordbrukets utvidgning.'
lot: 2
num: 6
---
