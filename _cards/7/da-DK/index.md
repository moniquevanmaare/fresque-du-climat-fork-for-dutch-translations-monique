---
title: 'CO2-udledninger'
backDescription: 'CO2 (kuldioxid) er den drivhusgas, vi mennesker udleder mest af. Menneskeskabte udledninger opstår ved forbrænding af fossile brændstoffer og afskovning.'
lot: 1
num: 7
---
