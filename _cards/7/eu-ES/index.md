---
title: 'CO₂ isurketak'
backDescription: 'CO₂-a, lehen berotegi-efektuko gas antropikoa da (gizakiak eragindakoa). CO₂ isurketak, energia fosilen erretzeak eta oihan soiltzeak sortzen dituzte.'
lot: 1
num: 7
---
