---
title: 'CO2-utslipp'
backDescription: 'CO2 (karbondioksid) er den største menneskeskapte klimagassen når det kommer til utslipp. Disse utslippene kommer fra forbruket av fossile brensler og avskoging.'
lot: 1
num: 7
---
