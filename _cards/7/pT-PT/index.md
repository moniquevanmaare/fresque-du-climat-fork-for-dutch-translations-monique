---
title: 'Emissões de CO2'
backDescription: 'O CO2 (dióxido de carbono) é o mais importante gás de efeito de estufa de origem antrópica (relacionado com as atividades humanas). As emissões de CO2 têm origem na queima de combustíveis fósseis e na desflorestação.'
lot: 1
num: 7
---
