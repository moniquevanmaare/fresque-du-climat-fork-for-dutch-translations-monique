---
title: 'Emisje CO2'
backDescription: 'CO2 jest głównym antropogenicznym (tzn. emitowanym przez człowieka) gazem cieplarnianym. Emisje CO2 są następstwem spalania paliw kopalnych oraz wylesiania.'
lot: 1
num: 7
---
