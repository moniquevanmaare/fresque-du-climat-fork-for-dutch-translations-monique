---
title: 'Agricoltura'
backDescription: "L'agricoltura è responsabile dell'emissione di un po' di CO2 e di una quantità rilevante di metano (bovini, risaie) e protossido di azoto (fertilizzante). In totale, se si include la deforestazione indotta, è responsabile del 25% dei gas serra."
lot: 2
num: 8
---
