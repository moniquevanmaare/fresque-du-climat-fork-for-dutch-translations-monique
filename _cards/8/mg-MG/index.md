---
title: 'Fambolena sy fiompiana'
backDescription: 'Ny fambolena sy ny fiompiana dia tompon''andraikitra amin''ny famotsorana CO2 kely,  "méthane" (Omby, tanimbary) betsaka ary "protoxyde d''azote" (zezika). Fehiny, 25%''ny entona maitso raha atao anatiny ny fandripahana ala aterany.'
lot: 2
num: 8
---
