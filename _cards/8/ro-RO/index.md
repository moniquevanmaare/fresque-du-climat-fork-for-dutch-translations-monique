---
title: "Agricultură"
backDescription: "Agricultura nu emite mult CO2, dar emite o cantitate semnificativă de metan (bovine, câmpuri de orez) și de oxid de azot (îngrășământ).
În total, dacă este inclusă despădurirea indusă, aceasta este responsabilă pentru 25% din gazele cu efect de seră."
lot: 2
num: 8
---
