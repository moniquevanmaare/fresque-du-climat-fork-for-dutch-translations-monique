---
title: 'Jordbruk'
backDescription: 'Jordbruket släpper inte ut så mycket koldioxid, men däremot stora mängder metan (från boskap och risfält) och lustgas (från gödsel). Totalt står jordbrukssektorn för 25% av de globala växthusgasustläppen, om en räknar med utsläpp från avskogning.'
lot: 2
num: 8
---
