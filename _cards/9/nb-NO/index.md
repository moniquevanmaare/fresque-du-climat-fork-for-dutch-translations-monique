---
title: 'Andre klimagasser'
backDescription: 'CO2 er ikke den eneste klimagassen. Blant andre er metan (CH4) og nitrogenoksid (N2O), to gasser som hovedsakelig avgis fra landbruket.'
lot: 2
num: 9
---
