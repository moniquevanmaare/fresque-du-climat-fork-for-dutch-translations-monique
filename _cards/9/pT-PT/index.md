---
title: "Outros GEE's"
backDescription: 'O CO2 não é o único gás com efeito de estufa. O metano (CH4) e o óxido nitroso (N2O), cujas emissões têm origem sobretudo na agricultura, são alguns dos exemplos.'
lot: 2
num: 9
---
