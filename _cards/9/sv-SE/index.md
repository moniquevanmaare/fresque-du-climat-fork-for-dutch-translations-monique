---
title: 'Andra växthusgaser'
backDescription: 'Koldioxid är inte den enda växthusgasen. Metan (CH4) och lustgas (N2O) är två andra växthusgaser. Dessa uppstår främst inom jordbruket.'
lot: 2
num: 9
---
