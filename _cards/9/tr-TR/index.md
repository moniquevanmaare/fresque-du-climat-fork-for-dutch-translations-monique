---
title: 'Diğer sera gazları'
backDescription: 'CO2 sera etkisine sahip olan tek gaz değildir. Diğer gazlar içerisinde tarım ve hayvancılık sebebiyle açığa çıkan metan (CH4) ve azot oksit (N2O) vardır.'
lot: 2
num: 9
---
