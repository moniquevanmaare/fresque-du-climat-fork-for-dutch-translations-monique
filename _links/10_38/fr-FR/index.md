---
linkId: '10_38'
fromCardId: 10
toCardId: 38
status: valid
---

Bien que les aérosols ne soient pas les seuls à rentrer dans la catégorie "Particule fines", chaque année, 391 000 personnes dans les pays de l'UE décèdent de la pollution de l'air, et elle cause 1.1 millions décès prématurés en Inde et en Chine.
