---
linkId: '11_12'
fromCardId: 11
toCardId: 12
status: invalid
---

On peut argumenter que les émissions de CO2 ont eu lieu dans l'atmosphère avant d'aller dans les puits de carbones. Ce lien est défendable. Mais l'idée était plutôt de faire un lien dans l'autre sens.
