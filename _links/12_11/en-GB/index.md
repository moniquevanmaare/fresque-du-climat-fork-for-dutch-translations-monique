---
linkId: '12_11'
fromCardId: 12
toCardId: 11
status: valid
---

the card n11 represents the concentration in CO2, understood in the atmosphere (it's written behind the card).
