---
linkId: '12_11'
fromCardId: 12
toCardId: 11
status: valid
---

La carte 11, c'est la concentration en CO2, sous-entendu "dans l'atmosphère" (qui est écrit sur la carte 12).
