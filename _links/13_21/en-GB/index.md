---
linkId: '13_21'
fromCardId: 13
toCardId: 21
status: simplified
---

In the simplified version, we make a direct link between cards 13 and 21. In this case, the role of card 21 changes and it becomes the temperature of the Earth, not only of the atmosphere.
