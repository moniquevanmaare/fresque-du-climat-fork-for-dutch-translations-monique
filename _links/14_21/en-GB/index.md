---
linkId: '14_21'
fromCardId: 14
toCardId: 21
status: valid
---

As mentionned on card 21, this is the temperature of the air, above ground, in average on the Earth.
