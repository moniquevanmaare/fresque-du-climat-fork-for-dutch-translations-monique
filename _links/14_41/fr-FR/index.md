---
linkId: '14_41'
fromCardId: 14
toCardId: 41
status: optional
---

Si l'on veut rester dans la même logique de conservation de l'énergie, il faudrait faire le lien de fonte à partir de bilan énergétique. Cependant pour des raisons pratiques, on peut préférer faire le lien à partir de la hausse de la température.
