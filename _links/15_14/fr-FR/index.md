---
linkId: '15_14'
fromCardId: 15
toCardId: 14
status: valid
---

Le texte au verso de la carte 14 ne laisse place à aucune ambiguïté. C'est pour cette raison qu'il faut supprimer ces deux cartes en même temps si on veut avoir une version simplifiée.
