---
linkId: '16_22'
fromCardId: 16
toCardId: 22
status: valid
---

Une fonte de 100 gigatonnes de glace par an équivaut à environ 0,28 mm par an d’élévation du niveau moyen des mers. Ainsi l'élévation du niveau de la mer est liée de 15 à 35% à la fonte des glaciers, selon les scénarios du GIEC. Le reste est liée de 30% à 35% à la dilatation de l'eau, ainsi qu'à la fonte des calottes glaciaires.
