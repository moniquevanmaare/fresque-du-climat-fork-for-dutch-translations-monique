---
linkId: '16_26'
fromCardId: 16
toCardId: 26
status: optional
---

Il est possible, dans certaines circonstances de forte chaleur, que la fonte trop rapide des glaciers provoque des crues. Mais le vrai sujet de préoccupation à propos des ces glaciers, c'est le fait qu'ils disparaissent progressivement, privant l'irrigation en aval d'un appoint d'eau en été.
