---
linkId: '16_31'
fromCardId: 16
toCardId: 31
status: valid
---

La fonte des glaciers menace l'approvisionnement en eau. En effet, l'importance relative des eaux de fonte des glaciers en été peut être considérable, contribuant par exemple à 25% des débits d'août dans les bassins drainant les Alpes européennes, avec une superficie d'environ 105 km2 et seulement 1% de couverture glaciaire. Leur disparition pourrait être catastrophique pour des villes situées dans les vallées arrosées par les rivières descendant des montagnes environnantes et pour la faune d'eau douce. L'eau de fonte des glaciers augmente également en importance pendant les sécheresses et les vagues de chaleur.
