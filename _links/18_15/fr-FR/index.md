---
linkId: '18_15'
fromCardId: 18
toCardId: 15
status: optional
---

Quand la banquise fond, une surface blanche est remplacé par une surface bleu marine, qui à un albédo plus faible, donc absorbe plus d'énergie. Cette relation n'est pas indispensable mais permet de mettre en avant une autre boucle de rétroaction du jeu.
