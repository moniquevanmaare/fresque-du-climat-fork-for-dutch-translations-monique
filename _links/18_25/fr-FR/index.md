---
linkId: '18_25'
fromCardId: 18
toCardId: 25
status: optional
---

Eh oui, le pauvre ours polaire, emblématique du changement climatique... Il ne s’agit pas d’un phénomène tellement significatif sur l'équilibre de la biodiversité terrestre, mais si les participants font ce lien, proposez-leur de dessiner un ours! ;-).
