---
linkId: '1_27'
fromCardId: 1
toCardId: 27
status: optional
---

With this link, we highlight all the degradations that humans are inflicting on marine life such as plastic pollution and overfishing. It's irrelevant to climate change, but it's interesting to make the connection anyway.
