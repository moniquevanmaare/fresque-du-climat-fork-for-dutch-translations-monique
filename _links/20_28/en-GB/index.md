---
linkId: '20_28'
fromCardId: 20
toCardId: 28
status: optional
---

The Vectors of disease card is generally linked to the Terrestrial Biodiversity card because disease vectors are a sub-part of biodiversity, but it can also be linked to the same causes as the biodiversity card, i.e. Disruption of the Water Cycle and Rising Air Temperatures.
