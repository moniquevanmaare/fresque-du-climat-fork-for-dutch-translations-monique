---
linkId: '23_27'
fromCardId: 23
toCardId: 27
status: optional
---

Les problèmes de calcification ne concernent pas seulement les ptéropodes et les coccolithophores. Ils peuvent aussi impacter le corail par exemple. Donc ce lien est tout à fait acceptable.
