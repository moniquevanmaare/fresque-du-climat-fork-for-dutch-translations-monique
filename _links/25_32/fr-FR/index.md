---
linkId: '25_32'
fromCardId: 25
toCardId: 32
status: valid
---

Les abeilles et les vers de terre, pour ne citer qu'eux, sont indispensables à l'agriculture.
