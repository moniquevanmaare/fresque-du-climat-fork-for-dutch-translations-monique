---
linkId: '26_38'
fromCardId: 26
toCardId: 38
status: valid
---

Les crues amènent souvent des problèmes sanitaires graves : les égouts débordent, le choléra menace.
