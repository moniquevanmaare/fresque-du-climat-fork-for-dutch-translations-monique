---
linkId: '2_9'
fromCardId: 2
toCardId: 9
status: optional
---

En réalité, l'industrie est responsable d'autant d'émissions de méthanes que l'agriculture à cause des émissions fugitives (les fuites de gaz naturels dans les pipelines). C'est un point qui est peu connu, donc cette relation n'est pas considérée comme incontournable. L'industrie émet également des HFC (fluides réfrigérants).
