---
linkId: '32_8'
fromCardId: 32
toCardId: 8
status: invalid
---

Effectivement, la baisse des rendements agricoles affecte l'agriculture. Mais la carte agriculture est plutôt là pour incarner les causes des émissions de GES.
