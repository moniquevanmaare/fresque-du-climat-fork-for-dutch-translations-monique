---
linkId: '35_10'
fromCardId: 35
toCardId: 10
status: optional
---

Les analyses par satellite montrent dans les fumées des quantités de particules toxiques qui posent des problèmes de santé publique.
