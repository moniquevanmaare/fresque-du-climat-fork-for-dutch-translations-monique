---
linkId: '36_30'
fromCardId: 36
toCardId: 30
status: optional
---

Une canicule, c'est la température. Une sécheresse, c'est l'humidité. Ceci étant dit, si il fait chaud, l'eau s'évapore et cela peut entraîner une sécheresse. Les deux vont de fait souvent ensemble.
