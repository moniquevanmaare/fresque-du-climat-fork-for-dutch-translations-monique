---
linkId: '3_26'
fromCardId: 3
toCardId: 26
status: optional
---

Soil artificialisation is also responsible for flooding because the soil is no longer able to absorb rain water.
