---
linkId: '40_1'
fromCardId: 40
toCardId: 1
status: optional
---

It's the final loop of the Club of Rome. All this will eventually regulate itself, but not necessarily peacefully. The players often make this link and sometimes propose to roll up the Fresk to connect the beginning and ending edges. Moreover, it is noteworthy that humankind appears in the first and last cards, but not in the middle of the Fresk.
