---
linkId: '40_1'
fromCardId: 40
toCardId: 1
status: optional
---

C'est "la boucle du club de Rome" ! Tout cela finira bien par se réguler, mais pas forcément de manière soft. Les participants font souvent ce lien, et proposent parfois de faire un rouleau avec la fresque pour mettre bord à bord la fin et le début. D'ailleurs, il est intéressant de remarquer qu'il y a des humains dans les cartes du début et celles de la fin, mais pas au milieu.
