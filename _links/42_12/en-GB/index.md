---
linkId: '42_12'
fromCardId: 42
toCardId: 12
status: valid
---

CO2 is absorbed at the surface of the ocean. Thermohaline circulation helps mix watersurface and deep ocean which is essential to the absoption of CO2 by the ocean.
