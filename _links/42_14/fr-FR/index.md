---
linkId: '42_14'
fromCardId: 42
toCardId: 14
status: valid
---

La circulation thermohaline a pour effet de mélanger l'eau de surface et l'océan profond, ce qui est indispensable à l'absorption de la chaleur par l'océan.
