---
linkId: '4_6'
fromCardId: 4
toCardId: 6
status: optional
---

La construction de route nécessite parfois de déforester, mais l'aspect unidimensionnelle de la route la rend quasiment négligeable devant la déforestation lié à l'agriculture.
