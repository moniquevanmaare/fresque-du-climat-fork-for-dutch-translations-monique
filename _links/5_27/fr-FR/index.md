---
linkId: '5_27'
fromCardId: 5
toCardId: 27
status: optional
---

La marée noire qu'a provoqué le naufrage du supertanker l'Amoco Cadiz est un exemple de lien entre énergie fossile et biodiversité.
