---
linkId: '6_26'
fromCardId: 6
toCardId: 26
status: optional
---

La végétation retient l'eau. Si on la coupe, cela facilite les crues.
