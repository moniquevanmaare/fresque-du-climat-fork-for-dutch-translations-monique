---
linkId: '6_30'
fromCardId: 6
toCardId: 30
status: optional
---

Deforestation can be the direct cause of droughts because trees stock a lot of water. If they are cut down, they no longer play their part as humidity regulators.
