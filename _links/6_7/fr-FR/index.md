---
linkId: '6_7'
fromCardId: 6
toCardId: 7
status: valid
---

La déforestation émet du CO2 car 93% du bois déforesté par l'homme est brûlé.
