---
linkId: '7_12'
fromCardId: 7
toCardId: 12
status: valid
---

Les deux cartes se mettent bord à bord pour faire un même graphique.
C'est ce qu'il faut réussir à leur faire trouver :

-   Regardez la carte 12. Qu'est-ce que vous voyez de bizarre ?
-   c'est écrit à l'envers !
-   exactement. C'est une énigme à résoudre. La solution est sur la table.
