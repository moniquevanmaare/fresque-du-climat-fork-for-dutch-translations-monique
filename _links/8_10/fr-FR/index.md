---
linkId: '8_10'
fromCardId: 8
toCardId: 10
status: optional
---

Dans l'absolu, c'est vrai : les épandages font des aérosols qui entrent en ligne de compte dans les pollutions locales aux alentours, mais c'est beaucoup moins important que la combustion incomplète des centrales thermiques.
