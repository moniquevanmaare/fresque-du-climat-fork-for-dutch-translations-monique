---
linkId: '8_7'
fromCardId: 8
toCardId: 7
status: optional
---

L'agriculture émet peu de CO2 à part à travers la déforestation. Son empreinte carbone est surtout dûe aux autres GES.
